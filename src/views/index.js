import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/layouts/dashboard.vue'
import HomeView from './home.vue'
import LoginView from './login.vue'
import ScheduleView from './schedule.vue'
import HistoricView from './historic.vue'
import UnassignedDispatchesView from './dispatches/unassigned.vue'
import AssignedDispatchesView from './dispatches/assigned.vue'
import ErrorView from './error.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      component: LoginView
    },
    {
      path: '/',
      component: Dashboard,
      children: [
        {
          path: '',
          component: HomeView
        }
      ]
    },
    {
      path: '/schedule',
      component: Dashboard,
      children: [
        {
          path: '',
          component: ScheduleView
        }
      ]
    },
    {
      path: '/historic',
      component: Dashboard,
      children: [
        {
          path: '',
          component: HistoricView
        }
      ]
    },
    {
      path: '/dispatches/unassigned',
      component: Dashboard,
      children: [
        {
          path: '',
          component: UnassignedDispatchesView
        }
      ]
    },
    {
      path: '/dispatches/assigned',
      component: Dashboard,
      children: [
        {
          path: '',
          component: AssignedDispatchesView
        }
      ]
    },
    {
      path: '*',
      component: ErrorView
    }
  ]
})
