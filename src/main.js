import Vue from 'vue'
import router from './views'
import Cover from 'vendors/cover/cover.js'
import 'vendors/cover/cover.css'
import 'vendors/cover/vendors/multiselect/style.css'
import 'vendors/cover/vendors/flatpickr/style.css'
import Multiselect from 'vendors/cover/vendors/multiselect'
import Flatpickr from 'vendors/cover/vendors/flatpickr'

// register globally
Vue.component('c-multiselect', Multiselect)
Vue.component('c-flatpickr', Flatpickr)

Vue.config.productionTip = false
Vue.use(Cover)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    siderOpen: false
  },
  router,
  render (h) {
    return (<router-view></router-view>)
  }
})
