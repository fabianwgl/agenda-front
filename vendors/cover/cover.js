(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vue"));
	else if(typeof define === 'function' && define.amd)
		define(["vue"], factory);
	else if(typeof exports === 'object')
		exports["Cover"] = factory(require("vue"));
	else
		root["Cover"] = factory(root["Vue"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_lRwf__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "lVK7");
/******/ })
/************************************************************************/
/******/ ({

/***/ "+6Bu":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

/***/ }),

/***/ "+E39":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("S82l")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "+ZMJ":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("lOnJ");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "+tPU":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("xGkn");
var global = __webpack_require__("7KvD");
var hide = __webpack_require__("hJx8");
var Iterators = __webpack_require__("/bQp");
var TO_STRING_TAG = __webpack_require__("dSzd")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "/bQp":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "/n6Q":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("zQR9");
__webpack_require__("+tPU");
module.exports = __webpack_require__("Kh4W").f('iterator');


/***/ }),

/***/ "/tum":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "06OY":
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__("3Eo+")('meta');
var isObject = __webpack_require__("EqjI");
var has = __webpack_require__("D2L2");
var setDesc = __webpack_require__("evD5").f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__("S82l")(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ "1kS7":
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ "3Eo+":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "3iQW":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "4mcu":
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "5++G":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "52gC":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "5QVw":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("BwfY"), __esModule: true };

/***/ }),

/***/ "77Pl":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("EqjI");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "7KvD":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "7UMu":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__("R9M2");
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ "880/":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("hJx8");


/***/ }),

/***/ "94VQ":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("Yobk");
var descriptor = __webpack_require__("X8DO");
var setToStringTag = __webpack_require__("e6n0");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("hJx8")(IteratorPrototype, __webpack_require__("dSzd")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "9bBU":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("mClu");
var $Object = __webpack_require__("FeBl").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "BwfY":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("fWfb");
__webpack_require__("M6a0");
__webpack_require__("OYls");
__webpack_require__("QWe/");
module.exports = __webpack_require__("FeBl").Symbol;


/***/ }),

/***/ "C4MV":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("9bBU"), __esModule: true };

/***/ }),

/***/ "Cdx3":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__("sB3e");
var $keys = __webpack_require__("lktj");

__webpack_require__("uqUo")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "D2L2":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "Dd5s":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Dd8w":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__("woOf");

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),

/***/ "DuR2":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "E61x":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "EGZi":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "EqjI":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "F8kQ":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "FeBl":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "GKcl":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "GXuI":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "HLMM":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Ibhu":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("D2L2");
var toIObject = __webpack_require__("TcQ7");
var arrayIndexOf = __webpack_require__("vFc/")(false);
var IE_PROTO = __webpack_require__("ax3d")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "J74g":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Kh4W":
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__("dSzd");


/***/ }),

/***/ "LKZe":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("NpIQ");
var createDesc = __webpack_require__("X8DO");
var toIObject = __webpack_require__("TcQ7");
var toPrimitive = __webpack_require__("MmMw");
var has = __webpack_require__("D2L2");
var IE8_DOM_DEFINE = __webpack_require__("SfB7");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("+E39") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "M6a0":
/***/ (function(module, exports) {



/***/ }),

/***/ "MU5D":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("R9M2");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "MmMw":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("EqjI");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "NpIQ":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "O4g8":
/***/ (function(module, exports) {

module.exports = true;


/***/ }),

/***/ "ON07":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("EqjI");
var document = __webpack_require__("7KvD").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "OYls":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("crlp")('asyncIterator');


/***/ }),

/***/ "PClm":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "PzxK":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("D2L2");
var toObject = __webpack_require__("sB3e");
var IE_PROTO = __webpack_require__("ax3d")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "QRG4":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("UuGF");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "QWe/":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("crlp")('observable');


/***/ }),

/***/ "R4wc":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__("kM2E");

$export($export.S + $export.F, 'Object', { assign: __webpack_require__("To3L") });


/***/ }),

/***/ "R9M2":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "RPLV":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("7KvD").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "Rrel":
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__("TcQ7");
var gOPN = __webpack_require__("n0T6").f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),

/***/ "S82l":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "SfB7":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("+E39") && !__webpack_require__("S82l")(function () {
  return Object.defineProperty(__webpack_require__("ON07")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "TcQ7":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("MU5D");
var defined = __webpack_require__("52gC");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "TfyY":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "To3L":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__("lktj");
var gOPS = __webpack_require__("1kS7");
var pIE = __webpack_require__("NpIQ");
var toObject = __webpack_require__("sB3e");
var IObject = __webpack_require__("MU5D");
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__("S82l")(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),

/***/ "UuGF":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "V3tA":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("R4wc");
module.exports = __webpack_require__("FeBl").Object.assign;


/***/ }),

/***/ "WIlk":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "X8DO":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "Xc4G":
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__("lktj");
var gOPS = __webpack_require__("1kS7");
var pIE = __webpack_require__("NpIQ");
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),

/***/ "Yobk":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("77Pl");
var dPs = __webpack_require__("qio6");
var enumBugKeys = __webpack_require__("xnc9");
var IE_PROTO = __webpack_require__("ax3d")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("ON07")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("RPLV").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "ZVDz":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Zgw8":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/**!
 * @fileOverview Kickass library to create and place poppers near their reference elements.
 * @version 1.13.0
 * @license
 * Copyright (c) 2016 Federico Zivolo and contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined';
var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
var timeoutDuration = 0;
for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
  if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
    timeoutDuration = 1;
    break;
  }
}

function microtaskDebounce(fn) {
  var called = false;
  return function () {
    if (called) {
      return;
    }
    called = true;
    window.Promise.resolve().then(function () {
      called = false;
      fn();
    });
  };
}

function taskDebounce(fn) {
  var scheduled = false;
  return function () {
    if (!scheduled) {
      scheduled = true;
      setTimeout(function () {
        scheduled = false;
        fn();
      }, timeoutDuration);
    }
  };
}

var supportsMicroTasks = isBrowser && window.Promise;

/**
* Create a debounced version of a method, that's asynchronously deferred
* but called in the minimum time possible.
*
* @method
* @memberof Popper.Utils
* @argument {Function} fn
* @returns {Function}
*/
var debounce = supportsMicroTasks ? microtaskDebounce : taskDebounce;

/**
 * Check if the given variable is a function
 * @method
 * @memberof Popper.Utils
 * @argument {Any} functionToCheck - variable to check
 * @returns {Boolean} answer to: is a function?
 */
function isFunction(functionToCheck) {
  var getType = {};
  return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

/**
 * Get CSS computed property of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Eement} element
 * @argument {String} property
 */
function getStyleComputedProperty(element, property) {
  if (element.nodeType !== 1) {
    return [];
  }
  // NOTE: 1 DOM access here
  var css = getComputedStyle(element, null);
  return property ? css[property] : css;
}

/**
 * Returns the parentNode or the host of the element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} parent
 */
function getParentNode(element) {
  if (element.nodeName === 'HTML') {
    return element;
  }
  return element.parentNode || element.host;
}

/**
 * Returns the scrolling parent of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} scroll parent
 */
function getScrollParent(element) {
  // Return body, `getScroll` will take care to get the correct `scrollTop` from it
  if (!element) {
    return document.body;
  }

  switch (element.nodeName) {
    case 'HTML':
    case 'BODY':
      return element.ownerDocument.body;
    case '#document':
      return element.body;
  }

  // Firefox want us to check `-x` and `-y` variations as well

  var _getStyleComputedProp = getStyleComputedProperty(element),
      overflow = _getStyleComputedProp.overflow,
      overflowX = _getStyleComputedProp.overflowX,
      overflowY = _getStyleComputedProp.overflowY;

  if (/(auto|scroll)/.test(overflow + overflowY + overflowX)) {
    return element;
  }

  return getScrollParent(getParentNode(element));
}

/**
 * Returns the offset parent of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Element} offset parent
 */
function getOffsetParent(element) {
  // NOTE: 1 DOM access here
  var offsetParent = element && element.offsetParent;
  var nodeName = offsetParent && offsetParent.nodeName;

  if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
    if (element) {
      return element.ownerDocument.documentElement;
    }

    return document.documentElement;
  }

  // .offsetParent will return the closest TD or TABLE in case
  // no offsetParent is present, I hate this job...
  if (['TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
    return getOffsetParent(offsetParent);
  }

  return offsetParent;
}

function isOffsetContainer(element) {
  var nodeName = element.nodeName;

  if (nodeName === 'BODY') {
    return false;
  }
  return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
}

/**
 * Finds the root node (document, shadowDOM root) of the given element
 * @method
 * @memberof Popper.Utils
 * @argument {Element} node
 * @returns {Element} root node
 */
function getRoot(node) {
  if (node.parentNode !== null) {
    return getRoot(node.parentNode);
  }

  return node;
}

/**
 * Finds the offset parent common to the two provided nodes
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element1
 * @argument {Element} element2
 * @returns {Element} common offset parent
 */
function findCommonOffsetParent(element1, element2) {
  // This check is needed to avoid errors in case one of the elements isn't defined for any reason
  if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
    return document.documentElement;
  }

  // Here we make sure to give as "start" the element that comes first in the DOM
  var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
  var start = order ? element1 : element2;
  var end = order ? element2 : element1;

  // Get common ancestor container
  var range = document.createRange();
  range.setStart(start, 0);
  range.setEnd(end, 0);
  var commonAncestorContainer = range.commonAncestorContainer;

  // Both nodes are inside #document

  if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
    if (isOffsetContainer(commonAncestorContainer)) {
      return commonAncestorContainer;
    }

    return getOffsetParent(commonAncestorContainer);
  }

  // one of the nodes is inside shadowDOM, find which one
  var element1root = getRoot(element1);
  if (element1root.host) {
    return findCommonOffsetParent(element1root.host, element2);
  } else {
    return findCommonOffsetParent(element1, getRoot(element2).host);
  }
}

/**
 * Gets the scroll value of the given element in the given side (top and left)
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @argument {String} side `top` or `left`
 * @returns {number} amount of scrolled pixels
 */
function getScroll(element) {
  var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

  var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
  var nodeName = element.nodeName;

  if (nodeName === 'BODY' || nodeName === 'HTML') {
    var html = element.ownerDocument.documentElement;
    var scrollingElement = element.ownerDocument.scrollingElement || html;
    return scrollingElement[upperSide];
  }

  return element[upperSide];
}

/*
 * Sum or subtract the element scroll values (left and top) from a given rect object
 * @method
 * @memberof Popper.Utils
 * @param {Object} rect - Rect object you want to change
 * @param {HTMLElement} element - The element from the function reads the scroll values
 * @param {Boolean} subtract - set to true if you want to subtract the scroll values
 * @return {Object} rect - The modifier rect object
 */
function includeScroll(rect, element) {
  var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  var scrollTop = getScroll(element, 'top');
  var scrollLeft = getScroll(element, 'left');
  var modifier = subtract ? -1 : 1;
  rect.top += scrollTop * modifier;
  rect.bottom += scrollTop * modifier;
  rect.left += scrollLeft * modifier;
  rect.right += scrollLeft * modifier;
  return rect;
}

/*
 * Helper to detect borders of a given element
 * @method
 * @memberof Popper.Utils
 * @param {CSSStyleDeclaration} styles
 * Result of `getStyleComputedProperty` on the given element
 * @param {String} axis - `x` or `y`
 * @return {number} borders - The borders size of the given axis
 */

function getBordersSize(styles, axis) {
  var sideA = axis === 'x' ? 'Left' : 'Top';
  var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

  return parseFloat(styles['border' + sideA + 'Width'], 10) + parseFloat(styles['border' + sideB + 'Width'], 10);
}

/**
 * Tells if you are running Internet Explorer 10
 * @method
 * @memberof Popper.Utils
 * @returns {Boolean} isIE10
 */
var isIE10 = undefined;

var isIE10$1 = function () {
  if (isIE10 === undefined) {
    isIE10 = navigator.appVersion.indexOf('MSIE 10') !== -1;
  }
  return isIE10;
};

function getSize(axis, body, html, computedStyle) {
  return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE10$1() ? html['offset' + axis] + computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')] + computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')] : 0);
}

function getWindowSizes() {
  var body = document.body;
  var html = document.documentElement;
  var computedStyle = isIE10$1() && getComputedStyle(html);

  return {
    height: getSize('Height', body, html, computedStyle),
    width: getSize('Width', body, html, computedStyle)
  };
}

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();





var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/**
 * Given element offsets, generate an output similar to getBoundingClientRect
 * @method
 * @memberof Popper.Utils
 * @argument {Object} offsets
 * @returns {Object} ClientRect like output
 */
function getClientRect(offsets) {
  return _extends({}, offsets, {
    right: offsets.left + offsets.width,
    bottom: offsets.top + offsets.height
  });
}

/**
 * Get bounding client rect of given element
 * @method
 * @memberof Popper.Utils
 * @param {HTMLElement} element
 * @return {Object} client rect
 */
function getBoundingClientRect(element) {
  var rect = {};

  // IE10 10 FIX: Please, don't ask, the element isn't
  // considered in DOM in some circumstances...
  // This isn't reproducible in IE10 compatibility mode of IE11
  if (isIE10$1()) {
    try {
      rect = element.getBoundingClientRect();
      var scrollTop = getScroll(element, 'top');
      var scrollLeft = getScroll(element, 'left');
      rect.top += scrollTop;
      rect.left += scrollLeft;
      rect.bottom += scrollTop;
      rect.right += scrollLeft;
    } catch (err) {}
  } else {
    rect = element.getBoundingClientRect();
  }

  var result = {
    left: rect.left,
    top: rect.top,
    width: rect.right - rect.left,
    height: rect.bottom - rect.top
  };

  // subtract scrollbar size from sizes
  var sizes = element.nodeName === 'HTML' ? getWindowSizes() : {};
  var width = sizes.width || element.clientWidth || result.right - result.left;
  var height = sizes.height || element.clientHeight || result.bottom - result.top;

  var horizScrollbar = element.offsetWidth - width;
  var vertScrollbar = element.offsetHeight - height;

  // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
  // we make this check conditional for performance reasons
  if (horizScrollbar || vertScrollbar) {
    var styles = getStyleComputedProperty(element);
    horizScrollbar -= getBordersSize(styles, 'x');
    vertScrollbar -= getBordersSize(styles, 'y');

    result.width -= horizScrollbar;
    result.height -= vertScrollbar;
  }

  return getClientRect(result);
}

function getOffsetRectRelativeToArbitraryNode(children, parent) {
  var isIE10 = isIE10$1();
  var isHTML = parent.nodeName === 'HTML';
  var childrenRect = getBoundingClientRect(children);
  var parentRect = getBoundingClientRect(parent);
  var scrollParent = getScrollParent(children);

  var styles = getStyleComputedProperty(parent);
  var borderTopWidth = parseFloat(styles.borderTopWidth, 10);
  var borderLeftWidth = parseFloat(styles.borderLeftWidth, 10);

  var offsets = getClientRect({
    top: childrenRect.top - parentRect.top - borderTopWidth,
    left: childrenRect.left - parentRect.left - borderLeftWidth,
    width: childrenRect.width,
    height: childrenRect.height
  });
  offsets.marginTop = 0;
  offsets.marginLeft = 0;

  // Subtract margins of documentElement in case it's being used as parent
  // we do this only on HTML because it's the only element that behaves
  // differently when margins are applied to it. The margins are included in
  // the box of the documentElement, in the other cases not.
  if (!isIE10 && isHTML) {
    var marginTop = parseFloat(styles.marginTop, 10);
    var marginLeft = parseFloat(styles.marginLeft, 10);

    offsets.top -= borderTopWidth - marginTop;
    offsets.bottom -= borderTopWidth - marginTop;
    offsets.left -= borderLeftWidth - marginLeft;
    offsets.right -= borderLeftWidth - marginLeft;

    // Attach marginTop and marginLeft because in some circumstances we may need them
    offsets.marginTop = marginTop;
    offsets.marginLeft = marginLeft;
  }

  if (isIE10 ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
    offsets = includeScroll(offsets, parent);
  }

  return offsets;
}

function getViewportOffsetRectRelativeToArtbitraryNode(element) {
  var html = element.ownerDocument.documentElement;
  var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
  var width = Math.max(html.clientWidth, window.innerWidth || 0);
  var height = Math.max(html.clientHeight, window.innerHeight || 0);

  var scrollTop = getScroll(html);
  var scrollLeft = getScroll(html, 'left');

  var offset = {
    top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
    left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
    width: width,
    height: height
  };

  return getClientRect(offset);
}

/**
 * Check if the given element is fixed or is inside a fixed parent
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @argument {Element} customContainer
 * @returns {Boolean} answer to "isFixed?"
 */
function isFixed(element) {
  var nodeName = element.nodeName;
  if (nodeName === 'BODY' || nodeName === 'HTML') {
    return false;
  }
  if (getStyleComputedProperty(element, 'position') === 'fixed') {
    return true;
  }
  return isFixed(getParentNode(element));
}

/**
 * Computed the boundaries limits and return them
 * @method
 * @memberof Popper.Utils
 * @param {HTMLElement} popper
 * @param {HTMLElement} reference
 * @param {number} padding
 * @param {HTMLElement} boundariesElement - Element used to define the boundaries
 * @returns {Object} Coordinates of the boundaries
 */
function getBoundaries(popper, reference, padding, boundariesElement) {
  // NOTE: 1 DOM access here
  var boundaries = { top: 0, left: 0 };
  var offsetParent = findCommonOffsetParent(popper, reference);

  // Handle viewport case
  if (boundariesElement === 'viewport') {
    boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent);
  } else {
    // Handle other cases based on DOM element used as boundaries
    var boundariesNode = void 0;
    if (boundariesElement === 'scrollParent') {
      boundariesNode = getScrollParent(getParentNode(reference));
      if (boundariesNode.nodeName === 'BODY') {
        boundariesNode = popper.ownerDocument.documentElement;
      }
    } else if (boundariesElement === 'window') {
      boundariesNode = popper.ownerDocument.documentElement;
    } else {
      boundariesNode = boundariesElement;
    }

    var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent);

    // In case of HTML, we need a different computation
    if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
      var _getWindowSizes = getWindowSizes(),
          height = _getWindowSizes.height,
          width = _getWindowSizes.width;

      boundaries.top += offsets.top - offsets.marginTop;
      boundaries.bottom = height + offsets.top;
      boundaries.left += offsets.left - offsets.marginLeft;
      boundaries.right = width + offsets.left;
    } else {
      // for all the other DOM elements, this one is good
      boundaries = offsets;
    }
  }

  // Add paddings
  boundaries.left += padding;
  boundaries.top += padding;
  boundaries.right -= padding;
  boundaries.bottom -= padding;

  return boundaries;
}

function getArea(_ref) {
  var width = _ref.width,
      height = _ref.height;

  return width * height;
}

/**
 * Utility used to transform the `auto` placement to the placement with more
 * available space.
 * @method
 * @memberof Popper.Utils
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
  var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

  if (placement.indexOf('auto') === -1) {
    return placement;
  }

  var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

  var rects = {
    top: {
      width: boundaries.width,
      height: refRect.top - boundaries.top
    },
    right: {
      width: boundaries.right - refRect.right,
      height: boundaries.height
    },
    bottom: {
      width: boundaries.width,
      height: boundaries.bottom - refRect.bottom
    },
    left: {
      width: refRect.left - boundaries.left,
      height: boundaries.height
    }
  };

  var sortedAreas = Object.keys(rects).map(function (key) {
    return _extends({
      key: key
    }, rects[key], {
      area: getArea(rects[key])
    });
  }).sort(function (a, b) {
    return b.area - a.area;
  });

  var filteredAreas = sortedAreas.filter(function (_ref2) {
    var width = _ref2.width,
        height = _ref2.height;
    return width >= popper.clientWidth && height >= popper.clientHeight;
  });

  var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

  var variation = placement.split('-')[1];

  return computedPlacement + (variation ? '-' + variation : '');
}

/**
 * Get offsets to the reference element
 * @method
 * @memberof Popper.Utils
 * @param {Object} state
 * @param {Element} popper - the popper element
 * @param {Element} reference - the reference element (the popper will be relative to this)
 * @returns {Object} An object containing the offsets which will be applied to the popper
 */
function getReferenceOffsets(state, popper, reference) {
  var commonOffsetParent = findCommonOffsetParent(popper, reference);
  return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent);
}

/**
 * Get the outer sizes of the given element (offset size + margins)
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element
 * @returns {Object} object containing width and height properties
 */
function getOuterSizes(element) {
  var styles = getComputedStyle(element);
  var x = parseFloat(styles.marginTop) + parseFloat(styles.marginBottom);
  var y = parseFloat(styles.marginLeft) + parseFloat(styles.marginRight);
  var result = {
    width: element.offsetWidth + y,
    height: element.offsetHeight + x
  };
  return result;
}

/**
 * Get the opposite placement of the given one
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement
 * @returns {String} flipped placement
 */
function getOppositePlacement(placement) {
  var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
  return placement.replace(/left|right|bottom|top/g, function (matched) {
    return hash[matched];
  });
}

/**
 * Get offsets to the popper
 * @method
 * @memberof Popper.Utils
 * @param {Object} position - CSS position the Popper will get applied
 * @param {HTMLElement} popper - the popper element
 * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
 * @param {String} placement - one of the valid placement options
 * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
 */
function getPopperOffsets(popper, referenceOffsets, placement) {
  placement = placement.split('-')[0];

  // Get popper node sizes
  var popperRect = getOuterSizes(popper);

  // Add position, width and height to our offsets object
  var popperOffsets = {
    width: popperRect.width,
    height: popperRect.height
  };

  // depending by the popper placement we have to compute its offsets slightly differently
  var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
  var mainSide = isHoriz ? 'top' : 'left';
  var secondarySide = isHoriz ? 'left' : 'top';
  var measurement = isHoriz ? 'height' : 'width';
  var secondaryMeasurement = !isHoriz ? 'height' : 'width';

  popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
  if (placement === secondarySide) {
    popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
  } else {
    popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
  }

  return popperOffsets;
}

/**
 * Mimics the `find` method of Array
 * @method
 * @memberof Popper.Utils
 * @argument {Array} arr
 * @argument prop
 * @argument value
 * @returns index or -1
 */
function find(arr, check) {
  // use native find if supported
  if (Array.prototype.find) {
    return arr.find(check);
  }

  // use `filter` to obtain the same behavior of `find`
  return arr.filter(check)[0];
}

/**
 * Return the index of the matching object
 * @method
 * @memberof Popper.Utils
 * @argument {Array} arr
 * @argument prop
 * @argument value
 * @returns index or -1
 */
function findIndex(arr, prop, value) {
  // use native findIndex if supported
  if (Array.prototype.findIndex) {
    return arr.findIndex(function (cur) {
      return cur[prop] === value;
    });
  }

  // use `find` + `indexOf` if `findIndex` isn't supported
  var match = find(arr, function (obj) {
    return obj[prop] === value;
  });
  return arr.indexOf(match);
}

/**
 * Loop trough the list of modifiers and run them in order,
 * each of them will then edit the data object.
 * @method
 * @memberof Popper.Utils
 * @param {dataObject} data
 * @param {Array} modifiers
 * @param {String} ends - Optional modifier name used as stopper
 * @returns {dataObject}
 */
function runModifiers(modifiers, data, ends) {
  var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex(modifiers, 'name', ends));

  modifiersToRun.forEach(function (modifier) {
    if (modifier['function']) {
      // eslint-disable-line dot-notation
      console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
    }
    var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
    if (modifier.enabled && isFunction(fn)) {
      // Add properties to offsets to make them a complete clientRect object
      // we do this before each modifier to make sure the previous one doesn't
      // mess with these values
      data.offsets.popper = getClientRect(data.offsets.popper);
      data.offsets.reference = getClientRect(data.offsets.reference);

      data = fn(data, modifier);
    }
  });

  return data;
}

/**
 * Updates the position of the popper, computing the new offsets and applying
 * the new style.<br />
 * Prefer `scheduleUpdate` over `update` because of performance reasons.
 * @method
 * @memberof Popper
 */
function update() {
  // if popper is destroyed, don't perform any further update
  if (this.state.isDestroyed) {
    return;
  }

  var data = {
    instance: this,
    styles: {},
    arrowStyles: {},
    attributes: {},
    flipped: false,
    offsets: {}
  };

  // compute reference element offsets
  data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference);

  // compute auto placement, store placement inside the data object,
  // modifiers will be able to edit `placement` if needed
  // and refer to originalPlacement to know the original value
  data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

  // store the computed placement inside `originalPlacement`
  data.originalPlacement = data.placement;

  // compute the popper offsets
  data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);
  data.offsets.popper.position = 'absolute';

  // run the modifiers
  data = runModifiers(this.modifiers, data);

  // the first `update` will call `onCreate` callback
  // the other ones will call `onUpdate` callback
  if (!this.state.isCreated) {
    this.state.isCreated = true;
    this.options.onCreate(data);
  } else {
    this.options.onUpdate(data);
  }
}

/**
 * Helper used to know if the given modifier is enabled.
 * @method
 * @memberof Popper.Utils
 * @returns {Boolean}
 */
function isModifierEnabled(modifiers, modifierName) {
  return modifiers.some(function (_ref) {
    var name = _ref.name,
        enabled = _ref.enabled;
    return enabled && name === modifierName;
  });
}

/**
 * Get the prefixed supported property name
 * @method
 * @memberof Popper.Utils
 * @argument {String} property (camelCase)
 * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
 */
function getSupportedPropertyName(property) {
  var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
  var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

  for (var i = 0; i < prefixes.length - 1; i++) {
    var prefix = prefixes[i];
    var toCheck = prefix ? '' + prefix + upperProp : property;
    if (typeof document.body.style[toCheck] !== 'undefined') {
      return toCheck;
    }
  }
  return null;
}

/**
 * Destroy the popper
 * @method
 * @memberof Popper
 */
function destroy() {
  this.state.isDestroyed = true;

  // touch DOM only if `applyStyle` modifier is enabled
  if (isModifierEnabled(this.modifiers, 'applyStyle')) {
    this.popper.removeAttribute('x-placement');
    this.popper.style.left = '';
    this.popper.style.position = '';
    this.popper.style.top = '';
    this.popper.style[getSupportedPropertyName('transform')] = '';
  }

  this.disableEventListeners();

  // remove the popper if user explicity asked for the deletion on destroy
  // do not use `remove` because IE11 doesn't support it
  if (this.options.removeOnDestroy) {
    this.popper.parentNode.removeChild(this.popper);
  }
  return this;
}

/**
 * Get the window associated with the element
 * @argument {Element} element
 * @returns {Window}
 */
function getWindow(element) {
  var ownerDocument = element.ownerDocument;
  return ownerDocument ? ownerDocument.defaultView : window;
}

function attachToScrollParents(scrollParent, event, callback, scrollParents) {
  var isBody = scrollParent.nodeName === 'BODY';
  var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
  target.addEventListener(event, callback, { passive: true });

  if (!isBody) {
    attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
  }
  scrollParents.push(target);
}

/**
 * Setup needed event listeners used to update the popper position
 * @method
 * @memberof Popper.Utils
 * @private
 */
function setupEventListeners(reference, options, state, updateBound) {
  // Resize event listener on window
  state.updateBound = updateBound;
  getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

  // Scroll event listener on scroll parents
  var scrollElement = getScrollParent(reference);
  attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
  state.scrollElement = scrollElement;
  state.eventsEnabled = true;

  return state;
}

/**
 * It will add resize/scroll events and start recalculating
 * position of the popper element when they are triggered.
 * @method
 * @memberof Popper
 */
function enableEventListeners() {
  if (!this.state.eventsEnabled) {
    this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
  }
}

/**
 * Remove event listeners used to update the popper position
 * @method
 * @memberof Popper.Utils
 * @private
 */
function removeEventListeners(reference, state) {
  // Remove resize event listener on window
  getWindow(reference).removeEventListener('resize', state.updateBound);

  // Remove scroll event listener on scroll parents
  state.scrollParents.forEach(function (target) {
    target.removeEventListener('scroll', state.updateBound);
  });

  // Reset state
  state.updateBound = null;
  state.scrollParents = [];
  state.scrollElement = null;
  state.eventsEnabled = false;
  return state;
}

/**
 * It will remove resize/scroll events and won't recalculate popper position
 * when they are triggered. It also won't trigger onUpdate callback anymore,
 * unless you call `update` method manually.
 * @method
 * @memberof Popper
 */
function disableEventListeners() {
  if (this.state.eventsEnabled) {
    cancelAnimationFrame(this.scheduleUpdate);
    this.state = removeEventListeners(this.reference, this.state);
  }
}

/**
 * Tells if a given input is a number
 * @method
 * @memberof Popper.Utils
 * @param {*} input to check
 * @return {Boolean}
 */
function isNumeric(n) {
  return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Set the style to the given popper
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element - Element to apply the style to
 * @argument {Object} styles
 * Object with a list of properties and values which will be applied to the element
 */
function setStyles(element, styles) {
  Object.keys(styles).forEach(function (prop) {
    var unit = '';
    // add unit if the value is numeric and is one of the following
    if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
      unit = 'px';
    }
    element.style[prop] = styles[prop] + unit;
  });
}

/**
 * Set the attributes to the given popper
 * @method
 * @memberof Popper.Utils
 * @argument {Element} element - Element to apply the attributes to
 * @argument {Object} styles
 * Object with a list of properties and values which will be applied to the element
 */
function setAttributes(element, attributes) {
  Object.keys(attributes).forEach(function (prop) {
    var value = attributes[prop];
    if (value !== false) {
      element.setAttribute(prop, attributes[prop]);
    } else {
      element.removeAttribute(prop);
    }
  });
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} data.styles - List of style properties - values to apply to popper element
 * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The same data object
 */
function applyStyle(data) {
  // any property present in `data.styles` will be applied to the popper,
  // in this way we can make the 3rd party modifiers add custom styles to it
  // Be aware, modifiers could override the properties defined in the previous
  // lines of this modifier!
  setStyles(data.instance.popper, data.styles);

  // any property present in `data.attributes` will be applied to the popper,
  // they will be set as HTML attributes of the element
  setAttributes(data.instance.popper, data.attributes);

  // if arrowElement is defined and arrowStyles has some properties
  if (data.arrowElement && Object.keys(data.arrowStyles).length) {
    setStyles(data.arrowElement, data.arrowStyles);
  }

  return data;
}

/**
 * Set the x-placement attribute before everything else because it could be used
 * to add margins to the popper margins needs to be calculated to get the
 * correct popper offsets.
 * @method
 * @memberof Popper.modifiers
 * @param {HTMLElement} reference - The reference element used to position the popper
 * @param {HTMLElement} popper - The HTML element used as popper.
 * @param {Object} options - Popper.js options
 */
function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
  // compute reference element offsets
  var referenceOffsets = getReferenceOffsets(state, popper, reference);

  // compute auto placement, store placement inside the data object,
  // modifiers will be able to edit `placement` if needed
  // and refer to originalPlacement to know the original value
  var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

  popper.setAttribute('x-placement', placement);

  // Apply `position` to popper before anything else because
  // without the position applied we can't guarantee correct computations
  setStyles(popper, { position: 'absolute' });

  return options;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function computeStyle(data, options) {
  var x = options.x,
      y = options.y;
  var popper = data.offsets.popper;

  // Remove this legacy support in Popper.js v2

  var legacyGpuAccelerationOption = find(data.instance.modifiers, function (modifier) {
    return modifier.name === 'applyStyle';
  }).gpuAcceleration;
  if (legacyGpuAccelerationOption !== undefined) {
    console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
  }
  var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

  var offsetParent = getOffsetParent(data.instance.popper);
  var offsetParentRect = getBoundingClientRect(offsetParent);

  // Styles
  var styles = {
    position: popper.position
  };

  // floor sides to avoid blurry text
  var offsets = {
    left: Math.floor(popper.left),
    top: Math.floor(popper.top),
    bottom: Math.floor(popper.bottom),
    right: Math.floor(popper.right)
  };

  var sideA = x === 'bottom' ? 'top' : 'bottom';
  var sideB = y === 'right' ? 'left' : 'right';

  // if gpuAcceleration is set to `true` and transform is supported,
  //  we use `translate3d` to apply the position to the popper we
  // automatically use the supported prefixed version if needed
  var prefixedProperty = getSupportedPropertyName('transform');

  // now, let's make a step back and look at this code closely (wtf?)
  // If the content of the popper grows once it's been positioned, it
  // may happen that the popper gets misplaced because of the new content
  // overflowing its reference element
  // To avoid this problem, we provide two options (x and y), which allow
  // the consumer to define the offset origin.
  // If we position a popper on top of a reference element, we can set
  // `x` to `top` to make the popper grow towards its top instead of
  // its bottom.
  var left = void 0,
      top = void 0;
  if (sideA === 'bottom') {
    top = -offsetParentRect.height + offsets.bottom;
  } else {
    top = offsets.top;
  }
  if (sideB === 'right') {
    left = -offsetParentRect.width + offsets.right;
  } else {
    left = offsets.left;
  }
  if (gpuAcceleration && prefixedProperty) {
    styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
    styles[sideA] = 0;
    styles[sideB] = 0;
    styles.willChange = 'transform';
  } else {
    // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
    var invertTop = sideA === 'bottom' ? -1 : 1;
    var invertLeft = sideB === 'right' ? -1 : 1;
    styles[sideA] = top * invertTop;
    styles[sideB] = left * invertLeft;
    styles.willChange = sideA + ', ' + sideB;
  }

  // Attributes
  var attributes = {
    'x-placement': data.placement
  };

  // Update `data` attributes, styles and arrowStyles
  data.attributes = _extends({}, attributes, data.attributes);
  data.styles = _extends({}, styles, data.styles);
  data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

  return data;
}

/**
 * Helper used to know if the given modifier depends from another one.<br />
 * It checks if the needed modifier is listed and enabled.
 * @method
 * @memberof Popper.Utils
 * @param {Array} modifiers - list of modifiers
 * @param {String} requestingName - name of requesting modifier
 * @param {String} requestedName - name of requested modifier
 * @returns {Boolean}
 */
function isModifierRequired(modifiers, requestingName, requestedName) {
  var requesting = find(modifiers, function (_ref) {
    var name = _ref.name;
    return name === requestingName;
  });

  var isRequired = !!requesting && modifiers.some(function (modifier) {
    return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
  });

  if (!isRequired) {
    var _requesting = '`' + requestingName + '`';
    var requested = '`' + requestedName + '`';
    console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
  }
  return isRequired;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function arrow(data, options) {
  var _data$offsets$arrow;

  // arrow depends on keepTogether in order to work
  if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
    return data;
  }

  var arrowElement = options.element;

  // if arrowElement is a string, suppose it's a CSS selector
  if (typeof arrowElement === 'string') {
    arrowElement = data.instance.popper.querySelector(arrowElement);

    // if arrowElement is not found, don't run the modifier
    if (!arrowElement) {
      return data;
    }
  } else {
    // if the arrowElement isn't a query selector we must check that the
    // provided DOM node is child of its popper node
    if (!data.instance.popper.contains(arrowElement)) {
      console.warn('WARNING: `arrow.element` must be child of its popper element!');
      return data;
    }
  }

  var placement = data.placement.split('-')[0];
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var isVertical = ['left', 'right'].indexOf(placement) !== -1;

  var len = isVertical ? 'height' : 'width';
  var sideCapitalized = isVertical ? 'Top' : 'Left';
  var side = sideCapitalized.toLowerCase();
  var altSide = isVertical ? 'left' : 'top';
  var opSide = isVertical ? 'bottom' : 'right';
  var arrowElementSize = getOuterSizes(arrowElement)[len];

  //
  // extends keepTogether behavior making sure the popper and its
  // reference have enough pixels in conjuction
  //

  // top/left side
  if (reference[opSide] - arrowElementSize < popper[side]) {
    data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
  }
  // bottom/right side
  if (reference[side] + arrowElementSize > popper[opSide]) {
    data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
  }
  data.offsets.popper = getClientRect(data.offsets.popper);

  // compute center of the popper
  var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

  // Compute the sideValue using the updated popper offsets
  // take popper margin in account because we don't have this info available
  var css = getStyleComputedProperty(data.instance.popper);
  var popperMarginSide = parseFloat(css['margin' + sideCapitalized], 10);
  var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width'], 10);
  var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

  // prevent arrowElement from being placed not contiguously to its popper
  sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

  data.arrowElement = arrowElement;
  data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

  return data;
}

/**
 * Get the opposite placement variation of the given one
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement variation
 * @returns {String} flipped placement variation
 */
function getOppositeVariation(variation) {
  if (variation === 'end') {
    return 'start';
  } else if (variation === 'start') {
    return 'end';
  }
  return variation;
}

/**
 * List of accepted placements to use as values of the `placement` option.<br />
 * Valid placements are:
 * - `auto`
 * - `top`
 * - `right`
 * - `bottom`
 * - `left`
 *
 * Each placement can have a variation from this list:
 * - `-start`
 * - `-end`
 *
 * Variations are interpreted easily if you think of them as the left to right
 * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
 * is right.<br />
 * Vertically (`left` and `right`), `start` is top and `end` is bottom.
 *
 * Some valid examples are:
 * - `top-end` (on top of reference, right aligned)
 * - `right-start` (on right of reference, top aligned)
 * - `bottom` (on bottom, centered)
 * - `auto-right` (on the side with more space available, alignment depends by placement)
 *
 * @static
 * @type {Array}
 * @enum {String}
 * @readonly
 * @method placements
 * @memberof Popper
 */
var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

// Get rid of `auto` `auto-start` and `auto-end`
var validPlacements = placements.slice(3);

/**
 * Given an initial placement, returns all the subsequent placements
 * clockwise (or counter-clockwise).
 *
 * @method
 * @memberof Popper.Utils
 * @argument {String} placement - A valid placement (it accepts variations)
 * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
 * @returns {Array} placements including their variations
 */
function clockwise(placement) {
  var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  var index = validPlacements.indexOf(placement);
  var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
  return counter ? arr.reverse() : arr;
}

var BEHAVIORS = {
  FLIP: 'flip',
  CLOCKWISE: 'clockwise',
  COUNTERCLOCKWISE: 'counterclockwise'
};

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function flip(data, options) {
  // if `inner` modifier is enabled, we can't use the `flip` modifier
  if (isModifierEnabled(data.instance.modifiers, 'inner')) {
    return data;
  }

  if (data.flipped && data.placement === data.originalPlacement) {
    // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
    return data;
  }

  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement);

  var placement = data.placement.split('-')[0];
  var placementOpposite = getOppositePlacement(placement);
  var variation = data.placement.split('-')[1] || '';

  var flipOrder = [];

  switch (options.behavior) {
    case BEHAVIORS.FLIP:
      flipOrder = [placement, placementOpposite];
      break;
    case BEHAVIORS.CLOCKWISE:
      flipOrder = clockwise(placement);
      break;
    case BEHAVIORS.COUNTERCLOCKWISE:
      flipOrder = clockwise(placement, true);
      break;
    default:
      flipOrder = options.behavior;
  }

  flipOrder.forEach(function (step, index) {
    if (placement !== step || flipOrder.length === index + 1) {
      return data;
    }

    placement = data.placement.split('-')[0];
    placementOpposite = getOppositePlacement(placement);

    var popperOffsets = data.offsets.popper;
    var refOffsets = data.offsets.reference;

    // using floor because the reference offsets may contain decimals we are not going to consider here
    var floor = Math.floor;
    var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

    var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
    var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
    var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
    var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

    var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

    // flip the variation if required
    var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
    var flippedVariation = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

    if (overlapsRef || overflowsBoundaries || flippedVariation) {
      // this boolean to detect any flip loop
      data.flipped = true;

      if (overlapsRef || overflowsBoundaries) {
        placement = flipOrder[index + 1];
      }

      if (flippedVariation) {
        variation = getOppositeVariation(variation);
      }

      data.placement = placement + (variation ? '-' + variation : '');

      // this object contains `position`, we want to preserve it along with
      // any additional property we may add in the future
      data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

      data = runModifiers(data.instance.modifiers, data, 'flip');
    }
  });
  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function keepTogether(data) {
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var placement = data.placement.split('-')[0];
  var floor = Math.floor;
  var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
  var side = isVertical ? 'right' : 'bottom';
  var opSide = isVertical ? 'left' : 'top';
  var measurement = isVertical ? 'width' : 'height';

  if (popper[side] < floor(reference[opSide])) {
    data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
  }
  if (popper[opSide] > floor(reference[side])) {
    data.offsets.popper[opSide] = floor(reference[side]);
  }

  return data;
}

/**
 * Converts a string containing value + unit into a px value number
 * @function
 * @memberof {modifiers~offset}
 * @private
 * @argument {String} str - Value + unit string
 * @argument {String} measurement - `height` or `width`
 * @argument {Object} popperOffsets
 * @argument {Object} referenceOffsets
 * @returns {Number|String}
 * Value in pixels, or original string if no values were extracted
 */
function toValue(str, measurement, popperOffsets, referenceOffsets) {
  // separate value from unit
  var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
  var value = +split[1];
  var unit = split[2];

  // If it's not a number it's an operator, I guess
  if (!value) {
    return str;
  }

  if (unit.indexOf('%') === 0) {
    var element = void 0;
    switch (unit) {
      case '%p':
        element = popperOffsets;
        break;
      case '%':
      case '%r':
      default:
        element = referenceOffsets;
    }

    var rect = getClientRect(element);
    return rect[measurement] / 100 * value;
  } else if (unit === 'vh' || unit === 'vw') {
    // if is a vh or vw, we calculate the size based on the viewport
    var size = void 0;
    if (unit === 'vh') {
      size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    } else {
      size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    }
    return size / 100 * value;
  } else {
    // if is an explicit pixel unit, we get rid of the unit and keep the value
    // if is an implicit unit, it's px, and we return just the value
    return value;
  }
}

/**
 * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
 * @function
 * @memberof {modifiers~offset}
 * @private
 * @argument {String} offset
 * @argument {Object} popperOffsets
 * @argument {Object} referenceOffsets
 * @argument {String} basePlacement
 * @returns {Array} a two cells array with x and y offsets in numbers
 */
function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
  var offsets = [0, 0];

  // Use height if placement is left or right and index is 0 otherwise use width
  // in this way the first offset will use an axis and the second one
  // will use the other one
  var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

  // Split the offset string to obtain a list of values and operands
  // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
  var fragments = offset.split(/(\+|\-)/).map(function (frag) {
    return frag.trim();
  });

  // Detect if the offset string contains a pair of values or a single one
  // they could be separated by comma or space
  var divider = fragments.indexOf(find(fragments, function (frag) {
    return frag.search(/,|\s/) !== -1;
  }));

  if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
    console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
  }

  // If divider is found, we divide the list of values and operands to divide
  // them by ofset X and Y.
  var splitRegex = /\s*,\s*|\s+/;
  var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

  // Convert the values with units to absolute pixels to allow our computations
  ops = ops.map(function (op, index) {
    // Most of the units rely on the orientation of the popper
    var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
    var mergeWithPrevious = false;
    return op
    // This aggregates any `+` or `-` sign that aren't considered operators
    // e.g.: 10 + +5 => [10, +, +5]
    .reduce(function (a, b) {
      if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
        a[a.length - 1] = b;
        mergeWithPrevious = true;
        return a;
      } else if (mergeWithPrevious) {
        a[a.length - 1] += b;
        mergeWithPrevious = false;
        return a;
      } else {
        return a.concat(b);
      }
    }, [])
    // Here we convert the string values into number values (in px)
    .map(function (str) {
      return toValue(str, measurement, popperOffsets, referenceOffsets);
    });
  });

  // Loop trough the offsets arrays and execute the operations
  ops.forEach(function (op, index) {
    op.forEach(function (frag, index2) {
      if (isNumeric(frag)) {
        offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
      }
    });
  });
  return offsets;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @argument {Number|String} options.offset=0
 * The offset value as described in the modifier description
 * @returns {Object} The data object, properly modified
 */
function offset(data, _ref) {
  var offset = _ref.offset;
  var placement = data.placement,
      _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var basePlacement = placement.split('-')[0];

  var offsets = void 0;
  if (isNumeric(+offset)) {
    offsets = [+offset, 0];
  } else {
    offsets = parseOffset(offset, popper, reference, basePlacement);
  }

  if (basePlacement === 'left') {
    popper.top += offsets[0];
    popper.left -= offsets[1];
  } else if (basePlacement === 'right') {
    popper.top += offsets[0];
    popper.left += offsets[1];
  } else if (basePlacement === 'top') {
    popper.left += offsets[0];
    popper.top -= offsets[1];
  } else if (basePlacement === 'bottom') {
    popper.left += offsets[0];
    popper.top += offsets[1];
  }

  data.popper = popper;
  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function preventOverflow(data, options) {
  var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

  // If offsetParent is the reference element, we really want to
  // go one step up and use the next offsetParent as reference to
  // avoid to make this modifier completely useless and look like broken
  if (data.instance.reference === boundariesElement) {
    boundariesElement = getOffsetParent(boundariesElement);
  }

  var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement);
  options.boundaries = boundaries;

  var order = options.priority;
  var popper = data.offsets.popper;

  var check = {
    primary: function primary(placement) {
      var value = popper[placement];
      if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
        value = Math.max(popper[placement], boundaries[placement]);
      }
      return defineProperty({}, placement, value);
    },
    secondary: function secondary(placement) {
      var mainSide = placement === 'right' ? 'left' : 'top';
      var value = popper[mainSide];
      if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
        value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
      }
      return defineProperty({}, mainSide, value);
    }
  };

  order.forEach(function (placement) {
    var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
    popper = _extends({}, popper, check[side](placement));
  });

  data.offsets.popper = popper;

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function shift(data) {
  var placement = data.placement;
  var basePlacement = placement.split('-')[0];
  var shiftvariation = placement.split('-')[1];

  // if shift shiftvariation is specified, run the modifier
  if (shiftvariation) {
    var _data$offsets = data.offsets,
        reference = _data$offsets.reference,
        popper = _data$offsets.popper;

    var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
    var side = isVertical ? 'left' : 'top';
    var measurement = isVertical ? 'width' : 'height';

    var shiftOffsets = {
      start: defineProperty({}, side, reference[side]),
      end: defineProperty({}, side, reference[side] + reference[measurement] - popper[measurement])
    };

    data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
  }

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by update method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function hide(data) {
  if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
    return data;
  }

  var refRect = data.offsets.reference;
  var bound = find(data.instance.modifiers, function (modifier) {
    return modifier.name === 'preventOverflow';
  }).boundaries;

  if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
    // Avoid unnecessary DOM access if visibility hasn't changed
    if (data.hide === true) {
      return data;
    }

    data.hide = true;
    data.attributes['x-out-of-boundaries'] = '';
  } else {
    // Avoid unnecessary DOM access if visibility hasn't changed
    if (data.hide === false) {
      return data;
    }

    data.hide = false;
    data.attributes['x-out-of-boundaries'] = false;
  }

  return data;
}

/**
 * @function
 * @memberof Modifiers
 * @argument {Object} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {Object} The data object, properly modified
 */
function inner(data) {
  var placement = data.placement;
  var basePlacement = placement.split('-')[0];
  var _data$offsets = data.offsets,
      popper = _data$offsets.popper,
      reference = _data$offsets.reference;

  var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

  var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

  popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

  data.placement = getOppositePlacement(placement);
  data.offsets.popper = getClientRect(popper);

  return data;
}

/**
 * Modifier function, each modifier can have a function of this type assigned
 * to its `fn` property.<br />
 * These functions will be called on each update, this means that you must
 * make sure they are performant enough to avoid performance bottlenecks.
 *
 * @function ModifierFn
 * @argument {dataObject} data - The data object generated by `update` method
 * @argument {Object} options - Modifiers configuration and options
 * @returns {dataObject} The data object, properly modified
 */

/**
 * Modifiers are plugins used to alter the behavior of your poppers.<br />
 * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
 * needed by the library.
 *
 * Usually you don't want to override the `order`, `fn` and `onLoad` props.
 * All the other properties are configurations that could be tweaked.
 * @namespace modifiers
 */
var modifiers = {
  /**
   * Modifier used to shift the popper on the start or end of its reference
   * element.<br />
   * It will read the variation of the `placement` property.<br />
   * It can be one either `-end` or `-start`.
   * @memberof modifiers
   * @inner
   */
  shift: {
    /** @prop {number} order=100 - Index used to define the order of execution */
    order: 100,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: shift
  },

  /**
   * The `offset` modifier can shift your popper on both its axis.
   *
   * It accepts the following units:
   * - `px` or unitless, interpreted as pixels
   * - `%` or `%r`, percentage relative to the length of the reference element
   * - `%p`, percentage relative to the length of the popper element
   * - `vw`, CSS viewport width unit
   * - `vh`, CSS viewport height unit
   *
   * For length is intended the main axis relative to the placement of the popper.<br />
   * This means that if the placement is `top` or `bottom`, the length will be the
   * `width`. In case of `left` or `right`, it will be the height.
   *
   * You can provide a single value (as `Number` or `String`), or a pair of values
   * as `String` divided by a comma or one (or more) white spaces.<br />
   * The latter is a deprecated method because it leads to confusion and will be
   * removed in v2.<br />
   * Additionally, it accepts additions and subtractions between different units.
   * Note that multiplications and divisions aren't supported.
   *
   * Valid examples are:
   * ```
   * 10
   * '10%'
   * '10, 10'
   * '10%, 10'
   * '10 + 10%'
   * '10 - 5vh + 3%'
   * '-10px + 5vh, 5px - 6%'
   * ```
   * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
   * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
   * > More on this [reading this issue](https://github.com/FezVrasta/popper.js/issues/373)
   *
   * @memberof modifiers
   * @inner
   */
  offset: {
    /** @prop {number} order=200 - Index used to define the order of execution */
    order: 200,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: offset,
    /** @prop {Number|String} offset=0
     * The offset value as described in the modifier description
     */
    offset: 0
  },

  /**
   * Modifier used to prevent the popper from being positioned outside the boundary.
   *
   * An scenario exists where the reference itself is not within the boundaries.<br />
   * We can say it has "escaped the boundaries" — or just "escaped".<br />
   * In this case we need to decide whether the popper should either:
   *
   * - detach from the reference and remain "trapped" in the boundaries, or
   * - if it should ignore the boundary and "escape with its reference"
   *
   * When `escapeWithReference` is set to`true` and reference is completely
   * outside its boundaries, the popper will overflow (or completely leave)
   * the boundaries in order to remain attached to the edge of the reference.
   *
   * @memberof modifiers
   * @inner
   */
  preventOverflow: {
    /** @prop {number} order=300 - Index used to define the order of execution */
    order: 300,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: preventOverflow,
    /**
     * @prop {Array} [priority=['left','right','top','bottom']]
     * Popper will try to prevent overflow following these priorities by default,
     * then, it could overflow on the left and on top of the `boundariesElement`
     */
    priority: ['left', 'right', 'top', 'bottom'],
    /**
     * @prop {number} padding=5
     * Amount of pixel used to define a minimum distance between the boundaries
     * and the popper this makes sure the popper has always a little padding
     * between the edges of its container
     */
    padding: 5,
    /**
     * @prop {String|HTMLElement} boundariesElement='scrollParent'
     * Boundaries used by the modifier, can be `scrollParent`, `window`,
     * `viewport` or any DOM element.
     */
    boundariesElement: 'scrollParent'
  },

  /**
   * Modifier used to make sure the reference and its popper stay near eachothers
   * without leaving any gap between the two. Expecially useful when the arrow is
   * enabled and you want to assure it to point to its reference element.
   * It cares only about the first axis, you can still have poppers with margin
   * between the popper and its reference element.
   * @memberof modifiers
   * @inner
   */
  keepTogether: {
    /** @prop {number} order=400 - Index used to define the order of execution */
    order: 400,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: keepTogether
  },

  /**
   * This modifier is used to move the `arrowElement` of the popper to make
   * sure it is positioned between the reference element and its popper element.
   * It will read the outer size of the `arrowElement` node to detect how many
   * pixels of conjuction are needed.
   *
   * It has no effect if no `arrowElement` is provided.
   * @memberof modifiers
   * @inner
   */
  arrow: {
    /** @prop {number} order=500 - Index used to define the order of execution */
    order: 500,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: arrow,
    /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
    element: '[x-arrow]'
  },

  /**
   * Modifier used to flip the popper's placement when it starts to overlap its
   * reference element.
   *
   * Requires the `preventOverflow` modifier before it in order to work.
   *
   * **NOTE:** this modifier will interrupt the current update cycle and will
   * restart it if it detects the need to flip the placement.
   * @memberof modifiers
   * @inner
   */
  flip: {
    /** @prop {number} order=600 - Index used to define the order of execution */
    order: 600,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: flip,
    /**
     * @prop {String|Array} behavior='flip'
     * The behavior used to change the popper's placement. It can be one of
     * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
     * placements (with optional variations).
     */
    behavior: 'flip',
    /**
     * @prop {number} padding=5
     * The popper will flip if it hits the edges of the `boundariesElement`
     */
    padding: 5,
    /**
     * @prop {String|HTMLElement} boundariesElement='viewport'
     * The element which will define the boundaries of the popper position,
     * the popper will never be placed outside of the defined boundaries
     * (except if keepTogether is enabled)
     */
    boundariesElement: 'viewport'
  },

  /**
   * Modifier used to make the popper flow toward the inner of the reference element.
   * By default, when this modifier is disabled, the popper will be placed outside
   * the reference element.
   * @memberof modifiers
   * @inner
   */
  inner: {
    /** @prop {number} order=700 - Index used to define the order of execution */
    order: 700,
    /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
    enabled: false,
    /** @prop {ModifierFn} */
    fn: inner
  },

  /**
   * Modifier used to hide the popper when its reference element is outside of the
   * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
   * be used to hide with a CSS selector the popper when its reference is
   * out of boundaries.
   *
   * Requires the `preventOverflow` modifier before it in order to work.
   * @memberof modifiers
   * @inner
   */
  hide: {
    /** @prop {number} order=800 - Index used to define the order of execution */
    order: 800,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: hide
  },

  /**
   * Computes the style that will be applied to the popper element to gets
   * properly positioned.
   *
   * Note that this modifier will not touch the DOM, it just prepares the styles
   * so that `applyStyle` modifier can apply it. This separation is useful
   * in case you need to replace `applyStyle` with a custom implementation.
   *
   * This modifier has `850` as `order` value to maintain backward compatibility
   * with previous versions of Popper.js. Expect the modifiers ordering method
   * to change in future major versions of the library.
   *
   * @memberof modifiers
   * @inner
   */
  computeStyle: {
    /** @prop {number} order=850 - Index used to define the order of execution */
    order: 850,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: computeStyle,
    /**
     * @prop {Boolean} gpuAcceleration=true
     * If true, it uses the CSS 3d transformation to position the popper.
     * Otherwise, it will use the `top` and `left` properties.
     */
    gpuAcceleration: true,
    /**
     * @prop {string} [x='bottom']
     * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
     * Change this if your popper should grow in a direction different from `bottom`
     */
    x: 'bottom',
    /**
     * @prop {string} [x='left']
     * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
     * Change this if your popper should grow in a direction different from `right`
     */
    y: 'right'
  },

  /**
   * Applies the computed styles to the popper element.
   *
   * All the DOM manipulations are limited to this modifier. This is useful in case
   * you want to integrate Popper.js inside a framework or view library and you
   * want to delegate all the DOM manipulations to it.
   *
   * Note that if you disable this modifier, you must make sure the popper element
   * has its position set to `absolute` before Popper.js can do its work!
   *
   * Just disable this modifier and define you own to achieve the desired effect.
   *
   * @memberof modifiers
   * @inner
   */
  applyStyle: {
    /** @prop {number} order=900 - Index used to define the order of execution */
    order: 900,
    /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
    enabled: true,
    /** @prop {ModifierFn} */
    fn: applyStyle,
    /** @prop {Function} */
    onLoad: applyStyleOnLoad,
    /**
     * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
     * @prop {Boolean} gpuAcceleration=true
     * If true, it uses the CSS 3d transformation to position the popper.
     * Otherwise, it will use the `top` and `left` properties.
     */
    gpuAcceleration: undefined
  }
};

/**
 * The `dataObject` is an object containing all the informations used by Popper.js
 * this object get passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
 * @name dataObject
 * @property {Object} data.instance The Popper.js instance
 * @property {String} data.placement Placement applied to popper
 * @property {String} data.originalPlacement Placement originally defined on init
 * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
 * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper.
 * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
 * @property {Object} data.styles Any CSS property defined here will be applied to the popper, it expects the JavaScript nomenclature (eg. `marginBottom`)
 * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow, it expects the JavaScript nomenclature (eg. `marginBottom`)
 * @property {Object} data.boundaries Offsets of the popper boundaries
 * @property {Object} data.offsets The measurements of popper, reference and arrow elements.
 * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
 * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
 * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
 */

/**
 * Default options provided to Popper.js constructor.<br />
 * These can be overriden using the `options` argument of Popper.js.<br />
 * To override an option, simply pass as 3rd argument an object with the same
 * structure of this object, example:
 * ```
 * new Popper(ref, pop, {
 *   modifiers: {
 *     preventOverflow: { enabled: false }
 *   }
 * })
 * ```
 * @type {Object}
 * @static
 * @memberof Popper
 */
var Defaults = {
  /**
   * Popper's placement
   * @prop {Popper.placements} placement='bottom'
   */
  placement: 'bottom',

  /**
   * Whether events (resize, scroll) are initially enabled
   * @prop {Boolean} eventsEnabled=true
   */
  eventsEnabled: true,

  /**
   * Set to true if you want to automatically remove the popper when
   * you call the `destroy` method.
   * @prop {Boolean} removeOnDestroy=false
   */
  removeOnDestroy: false,

  /**
   * Callback called when the popper is created.<br />
   * By default, is set to no-op.<br />
   * Access Popper.js instance with `data.instance`.
   * @prop {onCreate}
   */
  onCreate: function onCreate() {},

  /**
   * Callback called when the popper is updated, this callback is not called
   * on the initialization/creation of the popper, but only on subsequent
   * updates.<br />
   * By default, is set to no-op.<br />
   * Access Popper.js instance with `data.instance`.
   * @prop {onUpdate}
   */
  onUpdate: function onUpdate() {},

  /**
   * List of modifiers used to modify the offsets before they are applied to the popper.
   * They provide most of the functionalities of Popper.js
   * @prop {modifiers}
   */
  modifiers: modifiers
};

/**
 * @callback onCreate
 * @param {dataObject} data
 */

/**
 * @callback onUpdate
 * @param {dataObject} data
 */

// Utils
// Methods
var Popper = function () {
  /**
   * Create a new Popper.js instance
   * @class Popper
   * @param {HTMLElement|referenceObject} reference - The reference element used to position the popper
   * @param {HTMLElement} popper - The HTML element used as popper.
   * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
   * @return {Object} instance - The generated Popper.js instance
   */
  function Popper(reference, popper) {
    var _this = this;

    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    classCallCheck(this, Popper);

    this.scheduleUpdate = function () {
      return requestAnimationFrame(_this.update);
    };

    // make update() debounced, so that it only runs at most once-per-tick
    this.update = debounce(this.update.bind(this));

    // with {} we create a new object with the options inside it
    this.options = _extends({}, Popper.Defaults, options);

    // init state
    this.state = {
      isDestroyed: false,
      isCreated: false,
      scrollParents: []
    };

    // get reference and popper elements (allow jQuery wrappers)
    this.reference = reference && reference.jquery ? reference[0] : reference;
    this.popper = popper && popper.jquery ? popper[0] : popper;

    // Deep merge modifiers options
    this.options.modifiers = {};
    Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
      _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
    });

    // Refactoring modifiers' list (Object => Array)
    this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
      return _extends({
        name: name
      }, _this.options.modifiers[name]);
    })
    // sort the modifiers by order
    .sort(function (a, b) {
      return a.order - b.order;
    });

    // modifiers have the ability to execute arbitrary code when Popper.js get inited
    // such code is executed in the same order of its modifier
    // they could add new properties to their options configuration
    // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
    this.modifiers.forEach(function (modifierOptions) {
      if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
        modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
      }
    });

    // fire the first update to position the popper in the right place
    this.update();

    var eventsEnabled = this.options.eventsEnabled;
    if (eventsEnabled) {
      // setup event listeners, they will take care of update the position in specific situations
      this.enableEventListeners();
    }

    this.state.eventsEnabled = eventsEnabled;
  }

  // We can't use class properties because they don't get listed in the
  // class prototype and break stuff like Sinon stubs


  createClass(Popper, [{
    key: 'update',
    value: function update$$1() {
      return update.call(this);
    }
  }, {
    key: 'destroy',
    value: function destroy$$1() {
      return destroy.call(this);
    }
  }, {
    key: 'enableEventListeners',
    value: function enableEventListeners$$1() {
      return enableEventListeners.call(this);
    }
  }, {
    key: 'disableEventListeners',
    value: function disableEventListeners$$1() {
      return disableEventListeners.call(this);
    }

    /**
     * Schedule an update, it will run on the next UI update available
     * @method scheduleUpdate
     * @memberof Popper
     */


    /**
     * Collection of utilities useful when writing custom modifiers.
     * Starting from version 1.7, this method is available only if you
     * include `popper-utils.js` before `popper.js`.
     *
     * **DEPRECATION**: This way to access PopperUtils is deprecated
     * and will be removed in v2! Use the PopperUtils module directly instead.
     * Due to the high instability of the methods contained in Utils, we can't
     * guarantee them to follow semver. Use them at your own risk!
     * @static
     * @private
     * @type {Object}
     * @deprecated since version 1.8
     * @member Utils
     * @memberof Popper
     */

  }]);
  return Popper;
}();

/**
 * The `referenceObject` is an object that provides an interface compatible with Popper.js
 * and lets you use it as replacement of a real DOM node.<br />
 * You can use this method to position a popper relatively to a set of coordinates
 * in case you don't have a DOM node to use as reference.
 *
 * ```
 * new Popper(referenceObject, popperNode);
 * ```
 *
 * NB: This feature isn't supported in Internet Explorer 10
 * @name referenceObject
 * @property {Function} data.getBoundingClientRect
 * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
 * @property {number} data.clientWidth
 * An ES6 getter that will return the width of the virtual reference element.
 * @property {number} data.clientHeight
 * An ES6 getter that will return the height of the virtual reference element.
 */


Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
Popper.placements = placements;
Popper.Defaults = Defaults;

/* harmony default export */ __webpack_exports__["a"] = (Popper);
//# sourceMappingURL=popper.js.map

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("DuR2")))

/***/ }),

/***/ "Zrlr":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "Zzip":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("/n6Q"), __esModule: true };

/***/ }),

/***/ "ax3d":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("e8AB")('keys');
var uid = __webpack_require__("3Eo+");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "bOdI":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__("C4MV");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),

/***/ "crlp":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7KvD");
var core = __webpack_require__("FeBl");
var LIBRARY = __webpack_require__("O4g8");
var wksExt = __webpack_require__("Kh4W");
var defineProperty = __webpack_require__("evD5").f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),

/***/ "dSzd":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("e8AB")('wks');
var uid = __webpack_require__("3Eo+");
var Symbol = __webpack_require__("7KvD").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "e6n0":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("evD5").f;
var has = __webpack_require__("D2L2");
var TAG = __webpack_require__("dSzd")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "e8AB":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7KvD");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),

/***/ "efZv":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "evD5":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("77Pl");
var IE8_DOM_DEFINE = __webpack_require__("SfB7");
var toPrimitive = __webpack_require__("MmMw");
var dP = Object.defineProperty;

exports.f = __webpack_require__("+E39") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "fWfb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__("7KvD");
var has = __webpack_require__("D2L2");
var DESCRIPTORS = __webpack_require__("+E39");
var $export = __webpack_require__("kM2E");
var redefine = __webpack_require__("880/");
var META = __webpack_require__("06OY").KEY;
var $fails = __webpack_require__("S82l");
var shared = __webpack_require__("e8AB");
var setToStringTag = __webpack_require__("e6n0");
var uid = __webpack_require__("3Eo+");
var wks = __webpack_require__("dSzd");
var wksExt = __webpack_require__("Kh4W");
var wksDefine = __webpack_require__("crlp");
var enumKeys = __webpack_require__("Xc4G");
var isArray = __webpack_require__("7UMu");
var anObject = __webpack_require__("77Pl");
var toIObject = __webpack_require__("TcQ7");
var toPrimitive = __webpack_require__("MmMw");
var createDesc = __webpack_require__("X8DO");
var _create = __webpack_require__("Yobk");
var gOPNExt = __webpack_require__("Rrel");
var $GOPD = __webpack_require__("LKZe");
var $DP = __webpack_require__("evD5");
var $keys = __webpack_require__("lktj");
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__("n0T6").f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__("NpIQ").f = $propertyIsEnumerable;
  __webpack_require__("1kS7").f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__("O4g8")) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    if (it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    replacer = args[1];
    if (typeof replacer == 'function') $replacer = replacer;
    if ($replacer || !isArray(replacer)) replacer = function (key, value) {
      if ($replacer) value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__("hJx8")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),

/***/ "fZjL":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("jFbC"), __esModule: true };

/***/ }),

/***/ "fkB2":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("UuGF");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "ghX8":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "h65t":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("UuGF");
var defined = __webpack_require__("52gC");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "hJx8":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("evD5");
var createDesc = __webpack_require__("X8DO");
module.exports = __webpack_require__("+E39") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "jFbC":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("Cdx3");
module.exports = __webpack_require__("FeBl").Object.keys;


/***/ }),

/***/ "kM2E":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7KvD");
var core = __webpack_require__("FeBl");
var ctx = __webpack_require__("+ZMJ");
var hide = __webpack_require__("hJx8");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "lOnJ":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "lRwf":
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_lRwf__;

/***/ }),

/***/ "lVK7":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: ./src/styles/global.scss
var global = __webpack_require__("GKcl");
var global_default = /*#__PURE__*/__webpack_require__.n(global);

// EXTERNAL MODULE: ./src/directives/tooltip/style.scss
var tooltip_style = __webpack_require__("3iQW");
var style_default = /*#__PURE__*/__webpack_require__.n(tooltip_style);

// EXTERNAL MODULE: ./node_modules/babel-runtime/core-js/object/keys.js
var object_keys = __webpack_require__("fZjL");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/extends.js
var helpers_extends = __webpack_require__("Dd8w");
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/classCallCheck.js
var classCallCheck = __webpack_require__("Zrlr");
var classCallCheck_default = /*#__PURE__*/__webpack_require__.n(classCallCheck);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/createClass.js
var createClass = __webpack_require__("wxAW");
var createClass_default = /*#__PURE__*/__webpack_require__.n(createClass);

// EXTERNAL MODULE: ./node_modules/popper.js/dist/esm/popper.js
var popper = __webpack_require__("Zgw8");

// CONCATENATED MODULE: ./src/directives/tooltip/lib/tooltip.js





/**
 * Check if the given variable is a function
 * @method
 * @memberof Popper.Utils
 * @argument {Any} functionToCheck - variable to check
 * @returns {Boolean} answer to: is a function?
 */
function isFunction(functionToCheck) {
  var getType = {};
  return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

var DEFAULT_OPTIONS = {
  container: false,
  delay: 0,
  html: false,
  placement: 'top',
  title: '',
  template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
  trigger: 'hover focus',
  offset: 0
};

var tooltip_Tooltip = function () {
  /**
   * Create a new Tooltip.js instance
   * @class Tooltip
   * @param {HTMLElement} reference - The DOM node used as reference of the tooltip (it can be a jQuery element).
   * @param {Object} options
   * @param {String} options.placement=bottom
   *      Placement of the popper accepted values: `top(-start, -end), right(-start, -end), bottom(-start, -end),
   *      left(-start, -end)`
   * @param {HTMLElement|String|false} options.container=false - Append the tooltip to a specific element.
   * @param {Number|Object} options.delay=0
   *      Delay showing and hiding the tooltip (ms) - does not apply to manual trigger type.
   *      If a number is supplied, delay is applied to both hide/show.
   *      Object structure is: `{ show: 500, hide: 100 }`
   * @param {Boolean} options.html=false - Insert HTML into the tooltip. If false, the content will inserted with `textContent`.
   * @param {String|PlacementFunction} options.placement='top' - One of the allowed placements, or a function returning one of them.
   * @param {String} [options.template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>']
   *      Base HTML to used when creating the tooltip.
   *      The tooltip's `title` will be injected into the `.tooltip-inner` or `.tooltip__inner`.
   *      `.tooltip-arrow` or `.tooltip__arrow` will become the tooltip's arrow.
   *      The outermost wrapper element should have the `.tooltip` class.
   * @param {String|HTMLElement|TitleFunction} options.title='' - Default title value if `title` attribute isn't present.
   * @param {String} [options.trigger='hover focus']
   *      How tooltip is triggered - click, hover, focus, manual.
   *      You may pass multiple triggers; separate them with a space. `manual` cannot be combined with any other trigger.
   * @param {HTMLElement} options.boundariesElement
   *      The element used as boundaries for the tooltip. For more information refer to Popper.js'
   *      [boundariesElement docs](https://popper.js.org/popper-documentation.html)
   * @param {Number|String} options.offset=0 - Offset of the tooltip relative to its reference. For more information refer to Popper.js'
   *      [offset docs](https://popper.js.org/popper-documentation.html)
   * @param {Object} options.popperOptions={} - Popper options, will be passed directly to popper instance. For more information refer to Popper.js'
   *      [options docs](https://popper.js.org/popper-documentation.html)
   * @return {Object} instance - The generated tooltip instance
   */
  function Tooltip(reference, options) {
    classCallCheck_default()(this, Tooltip);

    _initialiseProps.call(this);

    // apply user options over default ones
    options = extends_default()({}, DEFAULT_OPTIONS, options);

    reference.jquery && (reference = reference[0]);

    // cache reference and options
    this.reference = reference;
    this.options = options;

    // get events list
    var events = typeof options.trigger === 'string' ? options.trigger.split(' ').filter(function (trigger) {
      return ['click', 'hover', 'focus'].indexOf(trigger) !== -1;
    }) : [];

    // set initial state
    this._isOpen = false;
    this._popperOptions = {};

    // set event listeners
    this._setEventListeners(reference, events, options);
  }

  //
  // Public methods
  //

  /**
   * Reveals an element's tooltip. This is considered a "manual" triggering of the tooltip.
   * Tooltips with zero-length titles are never displayed.
   * @method Tooltip#show
   * @memberof Tooltip
   */


  /**
   * Hides an element’s tooltip. This is considered a “manual” triggering of the tooltip.
   * @method Tooltip#hide
   * @memberof Tooltip
   */


  /**
   * Hides and destroys an element’s tooltip.
   * @method Tooltip#dispose
   * @memberof Tooltip
   */


  /**
   * Toggles an element’s tooltip. This is considered a “manual” triggering of the tooltip.
   * @method Tooltip#toggle
   * @memberof Tooltip
   */


  /**
   * Updates the tooltip's title content
   * @method Tooltip#updateTitleContent
   * @memberof Tooltip
   * @param {String|HTMLElement} title - The new content to use for the title
   */


  //
  // Defaults
  //


  //
  // Private methods
  //

  createClass_default()(Tooltip, [{
    key: '_create',


    /**
     * Creates a new tooltip node
     * @memberof Tooltip
     * @private
     * @param {HTMLElement} reference
     * @param {String} template
     * @param {String|HTMLElement|TitleFunction} title
     * @param {Boolean} allowHtml
     * @return {HTMLelement} tooltipNode
     */
    value: function _create(reference, template, title, allowHtml) {
      // create tooltip element
      var tooltipGenerator = window.document.createElement('div');
      tooltipGenerator.innerHTML = template.trim();
      var tooltipNode = tooltipGenerator.childNodes[0];

      // add unique ID to our tooltip (needed for accessibility reasons)
      tooltipNode.id = 'tooltip_' + Math.random().toString(36).substr(2, 10);

      // set initial `aria-hidden` state to `false` (it's visible!)
      tooltipNode.setAttribute('aria-hidden', 'false');

      // add title to tooltip
      var titleNode = tooltipGenerator.querySelector(this.innerSelector);
      this._addTitleContent(reference, title, allowHtml, titleNode);

      // return the generated tooltip node
      return tooltipNode;
    }
  }, {
    key: '_addTitleContent',
    value: function _addTitleContent(reference, title, allowHtml, titleNode) {
      if (title.nodeType === 1 || title.nodeType === 11) {
        // if title is a element node or document fragment, append it only if allowHtml is true
        allowHtml && titleNode.appendChild(title);
      } else if (isFunction(title)) {
        // if title is a function, call it and set textContent or innerHtml depending by `allowHtml` value
        var titleText = title.call(reference);
        allowHtml ? titleNode.innerHTML = titleText : titleNode.textContent = titleText;
      } else {
        // if it's just a simple text, set textContent or innerHtml depending by `allowHtml` value
        allowHtml ? titleNode.innerHTML = title : titleNode.textContent = title;
      }
    }
  }, {
    key: '_show',
    value: function _show(reference, options) {
      // don't show if it's already visible
      // or if it's not being showed
      if (this._isOpen && !this._isOpening) {
        return this;
      }
      this._isOpen = true;

      // if the tooltipNode already exists, just show it
      if (this._tooltipNode) {
        this._tooltipNode.style.display = '';
        this._tooltipNode.setAttribute('aria-hidden', 'false');
        this.popperInstance.update();
        return this;
      }

      // get title
      var title = reference.getAttribute('title') || options.title;

      // don't show tooltip if no title is defined
      if (!title) {
        return this;
      }

      // create tooltip node
      var tooltipNode = this._create(reference, options.template, title, options.html);

      // Add `aria-describedby` to our reference element for accessibility reasons
      reference.setAttribute('aria-describedby', tooltipNode.id);

      // append tooltip to container
      var container = this._findContainer(options.container, reference);

      this._append(tooltipNode, container);

      this._popperOptions = extends_default()({}, options.popperOptions, {
        placement: options.placement
      });

      this._popperOptions.modifiers = extends_default()({}, this._popperOptions.modifiers, {
        arrow: {
          element: this.arrowSelector
        },
        offset: {
          offset: options.offset
        }
      });

      if (options.boundariesElement) {
        this._popperOptions.modifiers.preventOverflow = {
          boundariesElement: options.boundariesElement
        };
      }

      this.popperInstance = new popper["a" /* default */](reference, tooltipNode, this._popperOptions);

      this._tooltipNode = tooltipNode;

      return this;
    }
  }, {
    key: '_hide',
    value: function _hide() /*reference, options*/{
      // don't hide if it's already hidden
      if (!this._isOpen) {
        return this;
      }

      this._isOpen = false;

      // hide tooltipNode
      this._tooltipNode.style.display = 'none';
      this._tooltipNode.setAttribute('aria-hidden', 'true');

      return this;
    }
  }, {
    key: '_dispose',
    value: function _dispose() {
      var _this = this;

      // remove event listeners first to prevent any unexpected behaviour
      this._events.forEach(function (_ref) {
        var func = _ref.func,
            event = _ref.event;

        _this.reference.removeEventListener(event, func);
      });
      this._events = [];

      if (this._tooltipNode) {
        this._hide();

        // destroy instance
        this.popperInstance.destroy();

        // destroy tooltipNode if removeOnDestroy is not set, as popperInstance.destroy() already removes the element
        if (!this.popperInstance.options.removeOnDestroy) {
          this._tooltipNode.parentNode.removeChild(this._tooltipNode);
          this._tooltipNode = null;
        }
      }
      return this;
    }
  }, {
    key: '_findContainer',
    value: function _findContainer(container, reference) {
      // if container is a query, get the relative element
      if (typeof container === 'string') {
        container = window.document.querySelector(container);
      } else if (container === false) {
        // if container is `false`, set it to reference parent
        container = reference.parentNode;
      }
      return container;
    }

    /**
     * Append tooltip to container
     * @memberof Tooltip
     * @private
     * @param {HTMLElement} tooltip
     * @param {HTMLElement|String|false} container
     */

  }, {
    key: '_append',
    value: function _append(tooltipNode, container) {
      container.appendChild(tooltipNode);
    }
  }, {
    key: '_setEventListeners',
    value: function _setEventListeners(reference, events, options) {
      var _this2 = this;

      var directEvents = [];
      var oppositeEvents = [];

      events.forEach(function (event) {
        switch (event) {
          case 'hover':
            directEvents.push('mouseenter');
            oppositeEvents.push('mouseleave');
            break;
          case 'focus':
            directEvents.push('focus');
            oppositeEvents.push('blur');
            break;
          case 'click':
            directEvents.push('click');
            oppositeEvents.push('click');
            break;
        }
      });

      // schedule show tooltip
      directEvents.forEach(function (event) {
        var func = function func(evt) {
          if (_this2._isOpening === true) {
            return;
          }
          evt.usedByTooltip = true;
          _this2._scheduleShow(reference, options.delay, options, evt);
        };
        _this2._events.push({ event: event, func: func });
        reference.addEventListener(event, func);
      });

      // schedule hide tooltip
      oppositeEvents.forEach(function (event) {
        var func = function func(evt) {
          if (evt.usedByTooltip === true) {
            return;
          }
          _this2._scheduleHide(reference, options.delay, options, evt);
        };
        _this2._events.push({ event: event, func: func });
        reference.addEventListener(event, func);
      });
    }
  }, {
    key: '_scheduleShow',
    value: function _scheduleShow(reference, delay, options /*, evt */) {
      var _this3 = this;

      this._isOpening = true;
      // defaults to 0
      var computedDelay = delay && delay.show || delay || 0;
      this._showTimeout = window.setTimeout(function () {
        return _this3._show(reference, options);
      }, computedDelay);
    }
  }, {
    key: '_scheduleHide',
    value: function _scheduleHide(reference, delay, options, evt) {
      var _this4 = this;

      this._isOpening = false;
      // defaults to 0
      var computedDelay = delay && delay.hide || delay || 0;
      window.setTimeout(function () {
        window.clearTimeout(_this4._showTimeout);
        if (_this4._isOpen === false) {
          return;
        }
        if (!document.body.contains(_this4._tooltipNode)) {
          return;
        }

        // if we are hiding because of a mouseleave, we must check that the new
        // reference isn't the tooltip, because in this case we don't want to hide it
        if (evt.type === 'mouseleave') {
          var isSet = _this4._setTooltipNodeEvent(evt, reference, delay, options);

          // if we set the new event, don't hide the tooltip yet
          // the new event will take care to hide it if necessary
          if (isSet) {
            return;
          }
        }

        _this4._hide(reference, options);
      }, computedDelay);
    }
  }, {
    key: '_updateTitleContent',
    value: function _updateTitleContent(title) {
      if (typeof this._tooltipNode === 'undefined') {
        if (typeof this.options.title !== 'undefined') {
          this.options.title = title;
        }
        return;
      }
      var titleNode = this._tooltipNode.parentNode.querySelector(this.innerSelector);
      this._clearTitleContent(titleNode, this.options.html, this.reference.getAttribute('title') || this.options.title);
      this._addTitleContent(this.reference, title, this.options.html, titleNode);
      this.options.title = title;
    }
  }, {
    key: '_clearTitleContent',
    value: function _clearTitleContent(titleNode, allowHtml, lastTitle) {
      if (lastTitle.nodeType === 1 || lastTitle.nodeType === 11) {
        allowHtml && titleNode.removeChild(lastTitle);
      } else {
        allowHtml ? titleNode.innerHTML = '' : titleNode.textContent = '';
      }
    }
  }]);

  return Tooltip;
}();

/**
 * Placement function, its context is the Tooltip instance.
 * @memberof Tooltip
 * @callback PlacementFunction
 * @param {HTMLElement} tooltip - tooltip DOM node.
 * @param {HTMLElement} reference - reference DOM node.
 * @return {String} placement - One of the allowed placement options.
 */

/**
 * Title function, its context is the Tooltip instance.
 * @memberof Tooltip
 * @callback TitleFunction
 * @return {String} placement - The desired title.
 */


var _initialiseProps = function _initialiseProps() {
  var _this5 = this;

  this.show = function () {
    return _this5._show(_this5.reference, _this5.options);
  };

  this.hide = function () {
    return _this5._hide();
  };

  this.dispose = function () {
    return _this5._dispose();
  };

  this.toggle = function () {
    if (_this5._isOpen) {
      return _this5.hide();
    } else {
      return _this5.show();
    }
  };

  this.updateTitleContent = function (title) {
    return _this5._updateTitleContent(title);
  };

  this.arrowSelector = '.tooltip-arrow, .tooltip__arrow';
  this.innerSelector = '.tooltip-inner, .tooltip__inner';
  this._events = [];

  this._setTooltipNodeEvent = function (evt, reference, delay, options) {
    var relatedreference = evt.relatedreference || evt.toElement || evt.relatedTarget;

    var callback = function callback(evt2) {
      var relatedreference2 = evt2.relatedreference || evt2.toElement || evt2.relatedTarget;

      // Remove event listener after call
      _this5._tooltipNode.removeEventListener(evt.type, callback);

      // If the new reference is not the reference element
      if (!reference.contains(relatedreference2)) {
        // Schedule to hide tooltip
        _this5._scheduleHide(reference, options.delay, options, evt2);
      }
    };

    if (_this5._tooltipNode.contains(relatedreference)) {
      // listen to mouseleave on the tooltip element to be able to hide the tooltip
      _this5._tooltipNode.addEventListener(evt.type, callback);
      return true;
    }

    return false;
  };
};

/* harmony default export */ var tooltip = (tooltip_Tooltip);
// CONCATENATED MODULE: ./src/directives/tooltip/tooltip.js




var positions = ['top', 'top-start', 'top-end', 'left', 'left-start', 'left-end', 'bottom', 'bottom-start', 'bottom-end', 'right', 'right-start', 'right-end'];

var tirggers = ['hover', 'click', 'focus'];

var defaults = {
  placement: 'top',
  container: 'body',
  delay: 0,
  html: true,
  template: '\n    <div class="tooltip" role="tooltip">\n      <div class="tooltip__arrow"></div>\n      <div class="tooltip__inner"></div>\n    </div>',
  title: '',
  trigger: 'hover',
  offset: '0,4'
};

/* harmony default export */ var tooltip_tooltip = ({
  name: 'tooltip',
  bind: function bind(el, binding, vnode) {
    var _binding$arg = binding.arg,
        arg = _binding$arg === undefined ? 4 : _binding$arg,
        _binding$value = binding.value,
        value = _binding$value === undefined ? '' : _binding$value,
        modifiers = binding.modifiers;

    var options = extends_default()({}, defaults, {
      title: value,
      offset: '0,' + arg,
      placement: getPlacement(modifiers),
      trigger: getTrigger(modifiers)
    });
    el.__tooltip__ = new tooltip(el, options);
  },
  update: function update(el, binding, vnode, oldVnode) {
    var value = binding.value,
        oldValue = binding.oldValue;

    if (value !== oldValue && el.__tooltip__) {
      el.__tooltip__.updateTitleContent(value);
    }
  },
  unbind: function unbind(el, binding, vnode) {
    el.__tooltip__ && el.__tooltip__.dispose();
  }
});

/**
 * Get placement from modifiers
 * @param {Object} modifiers
 * @return {String} placement
 */
function getPlacement() {
  var modifiers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var keys = keys_default()(modifiers);
  var placement = 'top';

  for (var i = keys.length - 1; i >= 0; i--) {
    if (positions.indexOf(keys[i]) !== -1) {
      placement = keys[i];
      break;
    }
  }

  return placement;
}

/**
 * Get trigger from modifiers
 * @param {Object} modifiers
 * @return {String} trigger
 */
function getTrigger() {
  var modifiers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var keys = keys_default()(modifiers);
  var trigger = 'hover';

  for (var i = keys.length - 1; i >= 0; i--) {
    if (tirggers.indexOf(keys[i]) !== -1) {
      trigger = keys[i];
      break;
    }
  }

  return trigger;
}
// CONCATENATED MODULE: ./src/directives/tooltip/index.js



/* harmony default export */ var directives_tooltip = (tooltip_tooltip);
// EXTERNAL MODULE: ./src/components/layout/style.scss
var layout_style = __webpack_require__("F8kQ");
var layout_style_default = /*#__PURE__*/__webpack_require__.n(layout_style);

// EXTERNAL MODULE: ./node_modules/babel-helper-vue-jsx-merge-props/index.js
var babel_helper_vue_jsx_merge_props = __webpack_require__("nvbp");
var babel_helper_vue_jsx_merge_props_default = /*#__PURE__*/__webpack_require__.n(babel_helper_vue_jsx_merge_props);

// CONCATENATED MODULE: ./src/components/layout/app.js

/* harmony default export */ var app = ({
  name: 'c-app',
  functional: true,
  props: {
    siderType: {
      type: String
    },
    siderOpen: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        children = _ref.children;

    var wrapperClass = {
      'app': true,
      'has-mini-sider': props.siderType === 'mini',
      'is-sider-open': props.siderOpen
    };
    return h(
      'div',
      babel_helper_vue_jsx_merge_props_default()([{ 'class': wrapperClass }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/sider.js

/* harmony default export */ var sider = ({
  name: 'c-sider',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        slots = _ref.slots;

    var startSlot = slots().start;
    var contentSlot = slots().default;
    var endSlot = slots().end;
    return h(
      "aside",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__sider" }, data]),
      [startSlot ? h(
        "div",
        { "class": "app__sider__start" },
        [startSlot]
      ) : null, h(
        "div",
        { "class": "app__sider__content" },
        [contentSlot]
      ), endSlot ? h(
        "div",
        { "class": "app__sider__end" },
        [endSlot]
      ) : null]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/brand.js

/* harmony default export */ var brand = ({
  name: 'c-brand',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__brand" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/content.js

/* harmony default export */ var layout_content = ({
  name: 'c-content',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__content" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/header.js

/* harmony default export */ var header = ({
  name: 'c-header',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "header",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__header" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/body.js

/* harmony default export */ var body = ({
  name: 'c-body',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__body" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/main.js

/* harmony default export */ var main = ({
  name: 'c-main',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "main",
      babel_helper_vue_jsx_merge_props_default()([{
        attrs: { role: "main" },
        "class": "app__main" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/footer.js

/* harmony default export */ var layout_footer = ({
  name: 'c-footer',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        children = _ref.children;

    return h(
      "footer",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "app__footer" }, data]),
      [children]
    );
  }
});
// CONCATENATED MODULE: ./src/components/layout/index.js











// CONCATENATED MODULE: ./src/utils/helpers.js
var nestRE = /^(attrs|props|on|nativeOn|class|style|hook)$/;

var mergeData = function mergeData() {
  for (var _len = arguments.length, sources = Array(_len), _key = 0; _key < _len; _key++) {
    sources[_key] = arguments[_key];
  }

  return sources.reduce(function (a, b) {
    var aa = void 0,
        bb = void 0,
        key = void 0,
        nestedKey = void 0,
        temp = void 0;
    for (key in b) {
      aa = a[key];
      bb = b[key];
      if (aa && nestRE.test(key)) {
        // normalize class
        if (key === 'class') {
          if (typeof aa === 'string') {
            temp = aa;
            a[key] = aa = {};
            aa[temp] = true;
          }
          if (typeof bb === 'string') {
            temp = bb;
            b[key] = bb = {};
            bb[temp] = true;
          }
        }
        if (key === 'on' || key === 'nativeOn' || key === 'hook') {
          // merge functions
          for (nestedKey in bb) {
            aa[nestedKey] = mergeFn(aa[nestedKey], bb[nestedKey]);
          }
        } else if (Array.isArray(aa)) {
          a[key] = aa.concat(bb);
        } else if (Array.isArray(bb)) {
          a[key] = [aa].concat(bb);
        } else {
          for (nestedKey in bb) {
            aa[nestedKey] = bb[nestedKey];
          }
        }
      } else if (key === 'staticClass') {
        a[key] = a[key] ? a[key] + ' ' + b[key] : b[key];
      } else {
        a[key] = b[key];
      }
    }
    return a;
  }, {});
};

var mergeFn = function mergeFn(a, b) {
  return function () {
    a && a.apply(this, arguments);
    b && b.apply(this, arguments);
  };
};

var filterColor = function filterColor(color) {
  switch (color) {
    case 'primary':
      return '#7872ff';
    case 'info':
      return '#2196f3';
    case 'success':
      return '#4caf50';
    case 'danger':
      return '#f44336';
    case 'warning':
      return '#ff9800';
    default:
      return color;
  }
};

var range = function range(start, end) {
  var result = [];
  for (var i = start; i <= end; i++) {
    result.push(i);
  }
  return result;
};
// CONCATENATED MODULE: ./src/components/grid/col.js


/* harmony default export */ var grid_col = ({
  name: 'c-col',
  functional: true,
  props: {
    order: {
      type: [String, Number]
    },
    span: {
      type: [String, Number]
    },
    xs: {
      type: [String, Number]
    },
    sm: {
      type: [String, Number]
    },
    md: {
      type: [String, Number]
    },
    lg: {
      type: [String, Number]
    },
    xl: {
      type: [String, Number]
    },
    offset: {
      type: [String, Number]
    },
    xsOffset: {
      type: [String, Number]
    },
    smOffset: {
      type: [String, Number]
    },
    mdOffset: {
      type: [String, Number]
    },
    lgOffset: {
      type: [String, Number]
    },
    xlOffset: {
      type: [String, Number]
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        children = _ref.children;

    var wrapperCls = {};
    var style = {};

    // calc order
    if (props.order) {
      style.order = parseInt(props.order);
    }

    // calc sizes
    var span = Math.abs(props.span || props.xs);
    if (span) {
      wrapperCls['l-col-' + span] = true;
    } else if (!props.xs && !props.md && !props.lg && !props.xl) {
      wrapperCls['l-col'] = true;
    }
    // calc offset
    var offset = Math.abs(props.offset || props.xsOffset);
    if (offset) {
      wrapperCls['l-col-offset-' + offset] = true;
    }

    ['sm', 'md', 'lg', 'xl'].forEach(function (key) {
      if (props[key] || props[key] === '') {
        var size = Math.abs(props[key]);
        if (size === 0) {
          wrapperCls['l-col@' + key] = true;
        } else {
          wrapperCls['l-col-' + size + '@' + key] = true;
        }
      }
      if (props[key + 'Offset']) {
        var _offset = Math.abs(props[key + 'Offset']);
        if (_offset) {
          wrapperCls['l-col-offset-' + _offset + '@' + key] = true;
        }
      }
    });

    return h('div', mergeData({
      'class': wrapperCls,
      style: style
    }, data), children);
  }
});
// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/defineProperty.js
var defineProperty = __webpack_require__("bOdI");
var defineProperty_default = /*#__PURE__*/__webpack_require__.n(defineProperty);

// CONCATENATED MODULE: ./src/components/grid/row.js



/* harmony default export */ var row = ({
  name: 'c-row',
  functional: true,
  props: {
    gutter: {
      type: [String, Number],
      default: -1
    },
    align: {
      type: String,
      default: ''
    },
    justify: {
      type: String,
      default: ''
    },
    card: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var _class;

    var props = _ref.props,
        data = _ref.data,
        children = _ref.children,
        slots = _ref.slots;

    var gutter = parseFloat(props.gutter);
    var align = ['top', 'middle', 'bottom', 'stretch'].indexOf(props.align) !== -1 ? props.align : '';
    var justify = ['start', 'end', 'center', 'around', 'between'].indexOf(props.justify) !== -1 ? props.justify : '';
    var style = {};

    var colMap = function colMap(col) {
      if (!col.fnOptions || col.fnOptions.name !== 'c-col') return col;

      col.data = mergeData(col.data, {
        style: {
          paddingLeft: gutter + 'px',
          paddingRight: gutter + 'px'
        }
      });

      return col;
    };

    if (gutter >= 0) {
      style.marginLeft = '-' + gutter + 'px';
      style.marginRight = '-' + gutter + 'px';
    }

    return h('div', mergeData({
      staticClass: 'l-row',
      'class': (_class = {}, defineProperty_default()(_class, 'is-' + justify, !!justify), defineProperty_default()(_class, 'is-' + align, !!align), defineProperty_default()(_class, 'is-card', props.card), _class),
      style: style
    }, data), gutter >= 0 ? children.map(colMap) : children);
  }
});
// CONCATENATED MODULE: ./src/components/grid/index.js




// CONCATENATED MODULE: ./src/components/level/level.js

/* harmony default export */ var level = ({
  name: 'c-level',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        slots = _ref.slots;

    var centerSlot = slots().center;
    var leftSlot = slots().left;
    var rightSlot = slots().right;
    var defaultSlot = slots().default;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "l-level" }, data]),
      [centerSlot ? h(
        "div",
        { "class": "l-level__center" },
        [centerSlot]
      ) : null, leftSlot ? h(
        "div",
        { "class": "l-level__left" },
        [leftSlot]
      ) : null, rightSlot ? h(
        "div",
        { "class": "l-level__right" },
        [rightSlot]
      ) : null, defaultSlot]
    );
  }
});
// CONCATENATED MODULE: ./src/components/level/level-item.js

/* harmony default export */ var level_item = ({
  name: 'c-level-item',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        slots = _ref.slots;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "l-level__item" }, data]),
      [slots().default]
    );
  }
});
// CONCATENATED MODULE: ./src/components/level/index.js




// EXTERNAL MODULE: ./src/components/stack/style.scss
var stack_style = __webpack_require__("HLMM");
var stack_style_default = /*#__PURE__*/__webpack_require__.n(stack_style);

// CONCATENATED MODULE: ./src/components/stack/stack.js

/* harmony default export */ var stack = ({
  name: 'c-stack',
  functional: true,
  props: {
    row: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;

    var wrapperClass = {
      'l-stack': true,
      'l-stack--row': props.row
    };
    var startSlot = slots().start;
    var endSlot = slots().end;
    var contentSlot = slots().default;

    return h(
      'div',
      babel_helper_vue_jsx_merge_props_default()([{ 'class': wrapperClass }, data]),
      [startSlot ? h(
        'div',
        { 'class': 'l-stack__start' },
        [startSlot]
      ) : null, h(
        'div',
        { 'class': 'l-stack__content' },
        [contentSlot]
      ), endSlot ? h(
        'div',
        { 'class': 'l-stack__end' },
        [endSlot]
      ) : null]
    );
  }
});
// CONCATENATED MODULE: ./src/components/stack/index.js



/* harmony default export */ var components_stack = (stack);
// CONCATENATED MODULE: ./src/components/button/button.js


/* harmony default export */ var button_button = ({
  name: 'c-button',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'button'
    },
    type: {
      type: String
    },
    size: {
      type: String
    },
    block: {
      type: Boolean,
      default: false
    },
    outline: {
      type: Boolean,
      default: false
    },
    iconStart: {
      type: String,
      default: ''
    },
    iconEnd: {
      type: String,
      default: ''
    },
    smart: {
      type: Boolean,
      default: false
    },
    active: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;
    var tag = props.tag,
        type = props.type,
        outline = props.outline,
        size = props.size,
        block = props.block,
        smart = props.smart,
        active = props.active,
        iconStart = props.iconStart,
        iconEnd = props.iconEnd;

    var children = null;
    var content = slots().default;
    var wrapperCls = { 'btn': true };
    if (type) wrapperCls['btn--' + type] = true;
    if (size) wrapperCls['btn--' + size] = true;
    if (outline) wrapperCls['btn--outline'] = true;
    if (smart) wrapperCls['btn--smart'] = true;
    if (block) wrapperCls['btn--block'] = true;
    if (active) wrapperCls['is-active'] = true;

    if (iconStart || iconEnd) {
      console.log(iconStart);
      if (content) {
        children = h(
          'span',
          { 'class': 'i-text' },
          [iconStart && h('i', { 'class': iconStart }), h('span', [content]), iconEnd && h('i', { 'class': iconEnd })]
        );
      } else {
        children = h('i', { 'class': iconStart || iconEnd });
      }
    } else {
      children = content;
    }

    return h(tag, mergeData({
      'class': wrapperCls
    }, data), [children]);
  }
});
// CONCATENATED MODULE: ./src/components/button/button-group.js

/* harmony default export */ var button_group = ({
  name: 'c-button-group',
  functional: true,
  render: function render(h, _ref) {
    var slots = _ref.slots,
        data = _ref.data;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "btn-group" }, data]),
      [slots().default]
    );
  }
});
// CONCATENATED MODULE: ./src/components/button/index.js




// EXTERNAL MODULE: ./src/components/panel/style.scss
var panel_style = __webpack_require__("E61x");
var panel_style_default = /*#__PURE__*/__webpack_require__.n(panel_style);

// CONCATENATED MODULE: ./src/components/panel/index.js




/* harmony default export */ var panel = ({
  name: 'c-panel',
  functional: true,
  props: {
    title: {
      type: String
    },
    type: {
      type: String
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;
    var title = props.title,
        type = props.type;

    var control = slots().control;
    var content = slots().default;
    var footer = slots().footer;
    var hasHeader = title || control;
    var wrapperCls = defineProperty_default()({
      'panel': true
    }, 'panel panel--' + type, !!type);
    return h(
      'section',
      babel_helper_vue_jsx_merge_props_default()([{ 'class': wrapperCls }, data]),
      [hasHeader ? h(
        'header',
        { 'class': 'panel__header' },
        [h('h2', { 'class': 'panel__title', domProps: {
            'innerHTML': title
          }
        }), control ? h(
          'div',
          { 'class': 'panel__control' },
          [control]
        ) : null]
      ) : null, h(
        'div',
        { 'class': 'panel__body' },
        [content]
      ), footer ? h(
        'footer',
        { 'class': 'panel__footer' },
        [footer]
      ) : null]
    );
  }
});
// CONCATENATED MODULE: ./src/components/badge/index.js



/* harmony default export */ var badge = ({
  name: 'c-badge',
  functional: true,
  props: {
    type: {
      type: String
    },
    tag: {
      type: String,
      default: 'span'
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;
    var type = props.type,
        tag = props.tag;


    return h(tag, mergeData({
      staticClass: 'badge',
      'class': defineProperty_default()({}, 'badge--' + type, !!type)
    }, data), [slots().default]);
  }
});
// EXTERNAL MODULE: ./src/components/tag/style.scss
var tag_style = __webpack_require__("ghX8");
var tag_style_default = /*#__PURE__*/__webpack_require__.n(tag_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/tag/tag.vue
//
//
//
//
//
//
//
//
//

/* harmony default export */ var tag_tag = ({
  name: "c-tag",
  props: {
    type: {
      type: String,
      default: ""
    },
    closable: {
      type: Boolean,
      default: false
    },
    checkable: {
      type: Boolean,
      default: false
    },
    value: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      checked: false
    };
  },

  watch: {
    value: {
      immediate: true,
      handler: function handler(val) {
        this.checked = val;
      }
    }
  },
  computed: {
    wrapperClass: function wrapperClass() {
      var classes = {};

      if (this.checkable === "" || this.checkable) {
        classes["tag--clickable"] = true;
        if (!this.checked) return classes;
      }

      if (this.type) {
        classes["tag--" + this.type] = true;
      }

      return classes;
    }
  },
  methods: {
    handleClose: function handleClose(e) {
      e.stopPropagation();
      this.$emit("close");
    },
    handleClick: function handleClick() {
      this.checked = !this.checked;
      this.$emit("input", this.checked);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-07da03a4","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/tag/tag.vue
var tag_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{staticClass:"tag",class:_vm.wrapperClass,on:{"click":_vm.handleClick}},[_c('span',[_vm._t("default")],2),_vm._v(" "),(_vm.closable)?_c('a',{staticClass:"tag__close",on:{"click":_vm.handleClose}}):_vm._e()])}
var staticRenderFns = []

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/component-normalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  scriptExports = scriptExports || {}

  // ES6 modules interop
  var type = typeof scriptExports.default
  if (type === 'object' || type === 'function') {
    scriptExports = scriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/tag/tag.vue
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null

var Component = normalizeComponent(
  tag_tag,
  tag_render,
  staticRenderFns,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ var components_tag_tag = (Component.exports);

// CONCATENATED MODULE: ./src/components/tag/index.js



/* harmony default export */ var components_tag = (components_tag_tag);
// CONCATENATED MODULE: ./src/components/box/index.js


/* harmony default export */ var box = ({
  name: 'c-box',
  functional: true,
  props: {
    type: {
      type: String
    },
    title: {
      type: String
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;
    var type = props.type,
        title = props.title;

    var wrapperCls = defineProperty_default()({
      box: true
    }, "box--" + type, !!type);
    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": wrapperCls }, data]),
      [h(
        "header",
        { "class": "box__header" },
        [slots().header || title]
      ), h(
        "div",
        { "class": "box__body" },
        [slots().default]
      )]
    );
  }
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/note/note.vue

//
//
//
//
//
//
//
//
//

/* harmony default export */ var note = ({
  name: 'c-note',
  props: {
    type: {
      type: String
    },
    closeable: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    wrapperCls: function wrapperCls() {
      var _ref;

      return _ref = {}, defineProperty_default()(_ref, 'note--' + this.type, !!this.type), defineProperty_default()(_ref, 'has-close', this.closeable), _ref;
    }
  },
  destroyed: function destroyed() {
    this.$el.parentNode.removeChild(this.$el);
  },

  methods: {
    close: function close() {
      this.$emit('close');
      this.$destroy();
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-8c1fd6ae","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/note/note.vue
var note_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"note",class:_vm.wrapperCls},[(_vm.closeable)?_c('a',{staticClass:"note__close",attrs:{"role":"button"},on:{"click":_vm.close}},[_c('i',{staticClass:"i-remove"})]):_vm._e(),_vm._v(" "),_vm._t("default")],2)}
var note_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/note/note.vue
/* script */


/* template */

/* template functional */
var note___vue_template_functional__ = false
/* styles */
var note___vue_styles__ = null
/* scopeId */
var note___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var note___vue_module_identifier__ = null

var note_Component = normalizeComponent(
  note,
  note_render,
  note_staticRenderFns,
  note___vue_template_functional__,
  note___vue_styles__,
  note___vue_scopeId__,
  note___vue_module_identifier__
)

/* harmony default export */ var note_note = (note_Component.exports);

// CONCATENATED MODULE: ./src/components/note/index.js


/* harmony default export */ var components_note = (note_note);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/icon/icon.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var icon = ({
  name: "c-svgicon",
  props: {
    name: {
      type: String
    },
    size: {
      type: [Number, String],
      default: "md"
    }
  },
  computed: {
    currentSize: function currentSize() {
      var currentSize = 64;
      switch (this.size) {
        case "xs":
          currentSize = 16;
          break;
        case "sm":
          currentSize = 32;
          break;
        case "md":
          currentSize = 64;
          break;
        case "lg":
          currentSize = 96;
          break;
        case "xl":
          currentSize = 128;
          break;
        default:
          currentSize = parseFloat(this.size);
          break;
      }
      return currentSize;
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-05e67db9","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/icon/icon.vue
var icon_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('svg',{attrs:{"version":"1.1","xmlns":"http://www.w3.org/2000/svg","width":_vm.currentSize,"height":_vm.currentSize,"viewBox":"0 0 64 64"}},[(_vm.name === 'alert')?[_c('path',{attrs:{"d":"M32 0C14.4 0 0 14.4 0 32s14.4 32 32 32 32-14.4 32-32S49.6 0 32 0zm4 50h-8v-8h8v8zm0-15h-8V15h8v20z","fill-rule":"nonzero"}})]:(_vm.name === 'check')?[_c('path',{attrs:{"d":"M32 0C14.329 0 0 14.329 0 32s14.329 32 32 32 32-14.329 32-32S49.671 0 32 0zm15.86 21.51l-19.202 25c-.151.154-.4.49-.704.49-.317 0-.524-.224-.704-.406-.179-.182-10.891-10.63-10.891-10.63l-.207-.21a.829.829 0 0 1-.152-.449c0-.168.069-.322.152-.448.055-.056.096-.098.152-.168 1.063-1.134 3.216-3.431 3.354-3.571.18-.182.331-.42.663-.42.345 0 .566.294.731.462.166.168 6.212 6.064 6.212 6.064l15.364-20.028a.781.781 0 0 1 .484-.196c.179 0 .345.07.483.182l4.224 3.375c.11.14.18.309.18.49a.733.733 0 0 1-.139.463z","fill-rule":"nonzero"}})]:(_vm.name === 'cross')?[_c('path',{attrs:{"d":"M32 0C14.24 0 0 14.24 0 32c0 17.76 14.24 32 32 32 17.76 0 32-14.24 32-32C64 14.24 49.76 0 32 0zm16 43.52L43.52 48 32 36.48 20.48 48 16 43.52 27.52 32 16 20.48 20.48 16 32 27.52 43.52 16 48 20.48 36.48 32 48 43.52z","fill-rule":"nonzero"}})]:(_vm.name === 'minus')?[_c('path',{attrs:{"d":"M32 0C14.327 0 0 14.327 0 32c0 17.673 14.327 32 32 32 17.673 0 32-14.327 32-32C64 14.327 49.673 0 32 0zm16 35H16v-6h32v6z","fill-rule":"nonzero"}})]:_vm._e()],2)}
var icon_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/icon/icon.vue
/* script */


/* template */

/* template functional */
var icon___vue_template_functional__ = false
/* styles */
var icon___vue_styles__ = null
/* scopeId */
var icon___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var icon___vue_module_identifier__ = null

var icon_Component = normalizeComponent(
  icon,
  icon_render,
  icon_staticRenderFns,
  icon___vue_template_functional__,
  icon___vue_styles__,
  icon___vue_scopeId__,
  icon___vue_module_identifier__
)

/* harmony default export */ var icon_icon = (icon_Component.exports);

// CONCATENATED MODULE: ./src/components/icon/index.js

/* harmony default export */ var components_icon = (icon_icon);
// EXTERNAL MODULE: ./src/components/divider/style.scss
var divider_style = __webpack_require__("/tum");
var divider_style_default = /*#__PURE__*/__webpack_require__.n(divider_style);

// CONCATENATED MODULE: ./src/components/divider/divider.js

/* harmony default export */ var divider = ({
  name: 'c-divider',
  functional: true,
  props: {
    placement: {
      type: String,
      default: ''
    },
    vertical: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;

    var wrapperClass = ['divider'];
    switch (props.placement) {
      case 'start':
        wrapperClass.push('divider--start');
        break;
      case 'end':
        wrapperClass.push('divider--end');
        break;
      default:
        break;
    }

    if (props.vertical) {
      wrapperClass.push('divider--vertical');
    }

    return h(
      'div',
      babel_helper_vue_jsx_merge_props_default()([{ 'class': wrapperClass.join(' ') }, data]),
      [h('span', [slots().default])]
    );
  }
});
// CONCATENATED MODULE: ./src/components/divider/index.js



/* harmony default export */ var components_divider = (divider);
// EXTERNAL MODULE: ./src/components/progress/style.scss
var progress_style = __webpack_require__("ZVDz");
var progress_style_default = /*#__PURE__*/__webpack_require__.n(progress_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/progress/progress-bar.vue
//
//
//
//
//
//

var themes = ["primary", "info", "danger", "warning", "success"];
var sizes = ["sm", "lg"];

/* harmony default export */ var progress_bar = ({
  name: "c-progress-bar",

  props: {
    type: {
      type: String,
      default: ""
    },
    value: {
      type: [String, Number],
      default: 0
    },
    max: {
      type: [String, Number],
      default: 100
    },
    size: {
      type: String,
      default: ""
    },
    active: {
      type: Boolean,
      default: false
    }
  },

  computed: {
    rate: function rate() {
      return Math.abs(Math.round(parseFloat(this.value) / parseFloat(this.max) * 1000) / 10);
    },
    wrapperClass: function wrapperClass() {
      var cls = [];
      if (themes.indexOf(this.type) !== -1) {
        cls.push("progress--" + this.type);
      }

      if (sizes.indexOf(this.size) !== -1) {
        cls.push("progress--" + this.size);
      }

      if (this.active) {
        cls.push("is-active");
      }

      return cls;
    },
    barStyle: function barStyle() {
      var style = {
        width: this.rate + "%"
      };

      if (themes.indexOf(this.type) === -1) {
        style["background-color"] = this.type;
      }

      return style;
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-ee4c8fb0","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/progress/progress-bar.vue
var progress_bar_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"progress",class:_vm.wrapperClass},[_c('div',{staticClass:"progress__bar",style:(_vm.barStyle)})])}
var progress_bar_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/progress/progress-bar.vue
/* script */


/* template */

/* template functional */
var progress_bar___vue_template_functional__ = false
/* styles */
var progress_bar___vue_styles__ = null
/* scopeId */
var progress_bar___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var progress_bar___vue_module_identifier__ = null

var progress_bar_Component = normalizeComponent(
  progress_bar,
  progress_bar_render,
  progress_bar_staticRenderFns,
  progress_bar___vue_template_functional__,
  progress_bar___vue_styles__,
  progress_bar___vue_scopeId__,
  progress_bar___vue_module_identifier__
)

/* harmony default export */ var progress_progress_bar = (progress_bar_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/progress/progress-circle.vue
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var progress_circle = ({
  name: "c-progress-circle",
  props: {
    value: {
      type: [String, Number],
      default: 0
    },
    max: {
      type: [String, Number],
      default: 100
    },
    size: {
      type: [String, Number],
      default: 75
    },
    type: {
      type: String,
      default: "primary"
    },
    bg: {
      type: String,
      default: "#e5e5e5"
    },
    lineWidth: {
      type: [String, Number],
      default: 6
    },
    lineCap: {
      default: "round" // round, square
    }
  },
  computed: {
    wrapperStyle: function wrapperStyle() {
      var size = 75;

      if (this.size === "lg") {
        size = 120;
      } else if (this.size === "sm") {
        size = 40;
      } else if (!isNaN(parseFloat(this.size))) {
        size = parseFloat(this.size);
      }

      return {
        width: size + "px",
        height: size + "px"
      };
    },
    strokeColor: function strokeColor() {
      return filterColor(this.type);
    },
    radius: function radius() {
      return 50 - parseFloat(this.lineWidth) / 2;
    },
    pathString: function pathString() {
      return "M 50,50 m 0,-" + this.radius + "\n          a " + this.radius + "," + this.radius + " 0 1 1 0," + 2 * this.radius + "\n          a " + this.radius + "," + this.radius + " 0 1 1 0,-" + 2 * this.radius;
    },
    len: function len() {
      return Math.PI * 2 * this.radius;
    },
    rate: function rate() {
      return Math.abs(Math.round(parseFloat(this.value) / parseFloat(this.max) * 1000) / 10);
    },
    pathStyle: function pathStyle() {
      return {
        "stroke-dasharray": this.len + "px " + this.len + "px",
        "stroke-dashoffset": (100 - this.rate) / 100 * this.len + "px",
        transition: "stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease"
      };
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-49fcee45","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/progress/progress-circle.vue
var progress_circle_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"progress-circle",style:(_vm.wrapperStyle)},[_c('svg',{attrs:{"viewBox":"0 0 100 100"}},[_c('path',{attrs:{"d":_vm.pathString,"stroke":_vm.bg,"stroke-width":_vm.lineWidth,"fill-opacity":0}}),_vm._v(" "),_c('path',{style:(_vm.pathStyle),attrs:{"d":_vm.pathString,"stroke-linecap":_vm.lineCap,"stroke":_vm.strokeColor,"stroke-width":_vm.lineWidth,"fill-opacity":"0"}})]),_vm._v(" "),_c('div',{staticClass:"progress-circle__content"},[_vm._t("default")],2)])}
var progress_circle_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/progress/progress-circle.vue
/* script */


/* template */

/* template functional */
var progress_circle___vue_template_functional__ = false
/* styles */
var progress_circle___vue_styles__ = null
/* scopeId */
var progress_circle___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var progress_circle___vue_module_identifier__ = null

var progress_circle_Component = normalizeComponent(
  progress_circle,
  progress_circle_render,
  progress_circle_staticRenderFns,
  progress_circle___vue_template_functional__,
  progress_circle___vue_styles__,
  progress_circle___vue_scopeId__,
  progress_circle___vue_module_identifier__
)

/* harmony default export */ var progress_progress_circle = (progress_circle_Component.exports);

// CONCATENATED MODULE: ./src/components/progress/index.js





// CONCATENATED MODULE: ./src/components/dropdown/index.js

// @vue/component
/* harmony default export */ var dropdown = ({
  name: 'c-dropdown',

  props: {
    placement: {
      type: String,
      default: 'bottom-start'
    },
    value: {
      type: Boolean,
      default: false
    }
  },

  data: function data() {
    return {
      visible: false
    };
  },


  watch: {
    value: {
      immediate: true,
      handler: function handler(val) {
        if (val !== this.visible) {
          if (val) {
            this.show();
          } else {
            this.hide();
          }
        }
      }
    }
  },

  mounted: function mounted() {
    document.body.appendChild(this.$refs.dropdown);
    var parent = this.$el.parentNode;
    var trigger = this.$_trigger = this.$el.firstChild;
    parent.insertBefore(trigger, this.$el);
    parent.removeChild(this.$el);

    if (!this.$scopedSlots.trigger) {
      this.$_trigger.addEventListener('click', this.toggle);
    }
    document.addEventListener('click', this.$_onClickoutside);
  },
  beforeDestroy: function beforeDestroy() {
    this.$_destroy();
    if (!this.$scopedSlots.trigger) {
      this.$_trigger.removeEventListener('click', this.toggle);
    }
    document.removeEventListener('click', this.$_onClickoutside);
    document.body.removeChild(this.$refs.dropdown);
  },


  methods: {
    toggle: function toggle(e) {
      e.preventDefault();
      if (this.visible) {
        this.hide();
      } else {
        this.show();
      }
    },
    show: function show() {
      this.visible = true;
      this.$refs.dropdown.style.display = 'block';
      this.$_update();
      this.$emit('input', true);
    },
    hide: function hide() {
      this.visible = false;
      this.$refs.dropdown.style.display = 'none';
      this.$emit('input', false);
    },
    $_create: function $_create() {
      var refEl = this.$_trigger;
      var popperEl = this.$refs.dropdown;
      var options = {
        gpuAcceleration: false,
        placement: this.placement,
        modifiers: {
          offset: {
            offset: '0,4'
          }
        }
      };

      this.$_popperJS = new popper["a" /* default */](refEl, popperEl, options);
    },
    $_destroy: function $_destroy() {
      if (this.$_popperJS) {
        this.$_popperJS.destroy();
        this.$_popperJS = null;
      }
    },
    $_update: function $_update() {
      this.$_popperJS ? this.$_popperJS.update() : this.$_create();
    },
    $_onClickoutside: function $_onClickoutside(e) {
      if (!this.$_trigger.contains(e.target) && !this.$refs.dropdown.contains(e.target) && this.visible) {
        this.hide();
      }
    }
  },

  render: function render(h) {
    var trigger = void 0;
    var content = this.$slots.content && this.$slots.content[0];

    if (this.$scopedSlots.trigger) {
      trigger = this.$scopedSlots.trigger({
        show: this.show,
        hide: this.hide
      });
    } else {
      trigger = this.$slots.default && this.$slots.default[0];
    }

    if (!content) {
      throw new Error('You must provide a child node with [slot="content"] as dropdown content');
    }

    var dropdown = h('span', {
      style: {
        display: 'none',
        zIndex: 100
      },
      ref: 'dropdown'
    }, [content]);

    return h('span', {}, [trigger, dropdown]);
  }
});
// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/objectWithoutProperties.js
var objectWithoutProperties = __webpack_require__("+6Bu");
var objectWithoutProperties_default = /*#__PURE__*/__webpack_require__.n(objectWithoutProperties);

// CONCATENATED MODULE: ./src/components/dropmenu/index.js



var Dropmenu = {
  name: 'c-dropmenu',
  functional: true,
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        children = _ref.children;

    return h('ul', mergeData({
      staticClass: 'dropmenu'
    }, data), children);
  }
};

var DropmenuDivider = {
  name: 'c-dropmenu-divider',
  functional: true,
  render: function render(h, _ref2) {
    var props = _ref2.props,
        data = _ref2.data;

    return h('li', mergeData({
      staticClass: 'dropmenu__divider'
    }, data), []);
  }
};

var DropmenuItem = {
  name: 'c-dropmenu-item',
  functional: true,
  props: {
    title: {
      type: String,
      default: ''
    },
    icon: {
      type: String
    },
    after: {
      type: String
    },
    active: {
      type: Boolean,
      default: false
    },
    hasChildren: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref3) {
    var props = _ref3.props,
        data = _ref3.data,
        slots = _ref3.slots;

    var itemCls = ['dropmenu__item'];
    var linkCls = ['dropmenu__link'];
    var children = [];
    var iconChild = slots().icon;
    var afterChild = slots().after;

    var staticClass = data.staticClass,
        wrapperCls = data['class'],
        rest = objectWithoutProperties_default()(data, ['staticClass', 'class']);

    if (staticClass) {
      itemCls.push(staticClass);
    }

    if (props.hasChildren) {
      itemCls.push('has-children');
    }

    if (props.active) {
      linkCls.push('is-active');
    }

    if (props.icon) {
      children.push(h(
        'span',
        { 'class': 'dropmenu__icon' },
        [h('i', { 'class': props.icon })]
      ));
    } else if (iconChild) {
      children.push(h(
        'span',
        { 'class': 'dropmenu__icon' },
        [iconChild]
      ));
    }

    if (props.title) {
      children.push(h(
        'div',
        { 'class': 'dropmenu__text' },
        [props.title]
      ));
    }

    if (props.after) {
      children.push(h(
        'span',
        { 'class': 'dropmenu__after' },
        [props.after]
      ));
    } else if (afterChild) {
      children.push(h(
        'span',
        { 'class': 'dropmenu__after' },
        [afterChild]
      ));
    }

    return h('li', {
      staticClass: itemCls.join(' '),
      'class': wrapperCls
    }, [h('a', mergeData({
      staticClass: linkCls.join(' ')
    }, rest), children)]);
  }
};

// EXTERNAL MODULE: ./src/components/menu/style.scss
var menu_style = __webpack_require__("WIlk");
var menu_style_default = /*#__PURE__*/__webpack_require__.n(menu_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/menu/menu.vue

//
//
//
//
//
//

/* harmony default export */ var menu = ({
  name: "c-menu",
  props: {
    activeIndex: {
      type: String,
      default: ""
    },
    theme: {
      type: String
    },
    mini: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    activeIndex: function activeIndex(val) {
      if (val !== this.currentIndex) {
        this.currentIndex = val;
      }
    }
  },
  data: function data() {
    return {
      currentIndex: this.activeIndex,
      currentIndexPath: []
    };
  },

  computed: {
    wrapperCls: function wrapperCls() {
      var _ref;

      return _ref = {}, defineProperty_default()(_ref, "menu--mini", this.mini), defineProperty_default()(_ref, "menu--" + this.theme, !!this.theme), _ref;
    }
  },
  created: function created() {
    this.rootMenu = this;
  },

  methods: {
    select: function select(index) {
      this.currentIndex = index;
      this.$emit("select", this.currentIndex, this.currentIndexPath);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2badc20b","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/menu/menu.vue
var menu_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('ul',{staticClass:"menu",class:_vm.wrapperCls},[_vm._t("default")],2)}
var menu_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/menu/menu.vue
/* script */


/* template */

/* template functional */
var menu___vue_template_functional__ = false
/* styles */
var menu___vue_styles__ = null
/* scopeId */
var menu___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var menu___vue_module_identifier__ = null

var menu_Component = normalizeComponent(
  menu,
  menu_render,
  menu_staticRenderFns,
  menu___vue_template_functional__,
  menu___vue_styles__,
  menu___vue_scopeId__,
  menu___vue_module_identifier__
)

/* harmony default export */ var menu_menu = (menu_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/menu/menu-heading.vue
//
//
//
//
//
//
//
//
//

/* harmony default export */ var menu_heading = ({
  name: "c-menu-heading",
  props: {
    label: {
      type: String
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-8eba13a4","hasScoped":false,"optionsId":"0","buble":{"transforms":{"stripWithFunctional":true}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/menu/menu-heading.vue
var menu_heading_render = function (_h,_vm) {var _c=_vm._c;return _c('li',{staticClass:"menu__heading"},[_c('span',{domProps:{"innerHTML":_vm._s(_vm.props.label)}}),_vm._v(" "),_c('span',{staticClass:"menu__action"},[_vm._t("default")],2)])}
var menu_heading_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/menu/menu-heading.vue
/* script */


/* template */

/* template functional */
var menu_heading___vue_template_functional__ = true
/* styles */
var menu_heading___vue_styles__ = null
/* scopeId */
var menu_heading___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var menu_heading___vue_module_identifier__ = null

var menu_heading_Component = normalizeComponent(
  menu_heading,
  menu_heading_render,
  menu_heading_staticRenderFns,
  menu_heading___vue_template_functional__,
  menu_heading___vue_styles__,
  menu_heading___vue_scopeId__,
  menu_heading___vue_module_identifier__
)

/* harmony default export */ var menu_menu_heading = (menu_heading_Component.exports);

// CONCATENATED MODULE: ./src/components/menu/menu-mixin.js
/* harmony default export */ var menu_mixin = ({
  props: {
    index: {
      type: String
    }
  },
  computed: {
    rootMenu: function rootMenu() {
      var parent = this;
      while (parent && parent.$options.name !== 'c-menu') {
        parent = parent.$parent;
      }
      return parent;
    },
    parentMenu: function parentMenu() {
      var parent = this;
      while (parent && parent.$options.name !== 'c-submenu') {
        parent = parent.$parent;
      }
      return parent;
    },
    indexPath: function indexPath() {
      var path = [this.index];
      var parent = this.$parent;
      while (parent.$options.name !== 'c-menu') {
        if (parent.index) {
          path.unshift(parent.index);
        }
        parent = parent.$parent;
      }
      return path;
    }
  }
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/menu/menu-item.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var menu_item = ({
  name: "c-menu-item",
  mixins: [menu_mixin],
  inheritAttrs: false,
  props: {
    active: {
      type: Boolean,
      default: false
    },
    icon: {
      type: String
    },
    label: {
      type: String
    }
  },
  computed: {
    isActive: function isActive() {
      return this.active || this.rootMenu.currentIndex === this.index;
    }
  },
  watch: {
    isActive: {
      immediate: true,
      handler: function handler(val) {
        if (val) {
          this.rootMenu.currentIndexPath = this.indexPath;
        }
      }
    }
  },
  methods: {
    onClick: function onClick(e) {
      if (this.index) {
        this.rootMenu.select(this.index);
      }
      this.$emit("click", e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-34ad1f6d","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/menu/menu-item.vue
var menu_item_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{staticClass:"menu__item"},[_c('a',_vm._b({staticClass:"menu__link",class:{'is-active': _vm.isActive},on:{"click":_vm.onClick}},'a',_vm.$attrs,false),[(_vm.icon || _vm.$slots.icon)?_c('span',{staticClass:"menu__icon"},[(!_vm.$slots.icon)?_c('i',{class:_vm.icon}):_vm._e(),_vm._v(" "),_vm._t("icon")],2):_vm._e(),_vm._v(" "),_c('span',{staticClass:"menu__label",domProps:{"innerHTML":_vm._s(_vm.label)}}),_vm._v(" "),(_vm.$slots.default)?_c('span',{staticClass:"menu__after"},[_vm._t("default")],2):_vm._e()])])}
var menu_item_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/menu/menu-item.vue
/* script */


/* template */

/* template functional */
var menu_item___vue_template_functional__ = false
/* styles */
var menu_item___vue_styles__ = null
/* scopeId */
var menu_item___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var menu_item___vue_module_identifier__ = null

var menu_item_Component = normalizeComponent(
  menu_item,
  menu_item_render,
  menu_item_staticRenderFns,
  menu_item___vue_template_functional__,
  menu_item___vue_styles__,
  menu_item___vue_scopeId__,
  menu_item___vue_module_identifier__
)

/* harmony default export */ var menu_menu_item = (menu_item_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/menu/menu-divider.vue
//
//
//

/* harmony default export */ var menu_divider = ({
  name: 'c-menu-divider'
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-3e023b3e","hasScoped":false,"optionsId":"0","buble":{"transforms":{"stripWithFunctional":true}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/menu/menu-divider.vue
var menu_divider_render = function (_h,_vm) {var _c=_vm._c;return _c('li',{staticClass:"menu__divider"})}
var menu_divider_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/menu/menu-divider.vue
/* script */


/* template */

/* template functional */
var menu_divider___vue_template_functional__ = true
/* styles */
var menu_divider___vue_styles__ = null
/* scopeId */
var menu_divider___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var menu_divider___vue_module_identifier__ = null

var menu_divider_Component = normalizeComponent(
  menu_divider,
  menu_divider_render,
  menu_divider_staticRenderFns,
  menu_divider___vue_template_functional__,
  menu_divider___vue_styles__,
  menu_divider___vue_scopeId__,
  menu_divider___vue_module_identifier__
)

/* harmony default export */ var menu_menu_divider = (menu_divider_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/menu/submenu.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var submenu = ({
  name: 'c-submenu',
  mixins: [menu_mixin],
  props: {
    label: {
      type: String
    },
    icon: {
      type: String
    }
  },
  data: function data() {
    return {
      isOpen: false
    };
  },

  computed: {
    isActive: function isActive() {
      return this.rootMenu.currentIndexPath.indexOf(this.index) !== -1;
    }
  },
  methods: {
    onClick: function onClick(e) {
      this.isOpen = !this.isOpen;
      this.$emit('click', e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-5f70cffc","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/menu/submenu.vue
var submenu_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{staticClass:"menu__item has-children",class:{'is-open': _vm.isOpen, 'is-active': _vm.isActive}},[_c('a',{staticClass:"menu__link",attrs:{"role":"button"},on:{"click":_vm.onClick}},[(_vm.icon || _vm.$slots.icon)?_c('span',{staticClass:"menu__icon"},[(!_vm.$slots.icon)?_c('i',{class:_vm.icon}):_vm._e(),_vm._v(" "),_vm._t("icon")],2):_vm._e(),_vm._v(" "),_c('span',{staticClass:"menu__label",domProps:{"innerHTML":_vm._s(_vm.label)}}),_vm._v(" "),(_vm.$slots.after)?_c('span',{staticClass:"menu__after"},[_vm._t("after")],2):_vm._e()]),_vm._v(" "),_c('ul',{staticClass:"menu__sub"},[_vm._t("default")],2)])}
var submenu_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/menu/submenu.vue
/* script */


/* template */

/* template functional */
var submenu___vue_template_functional__ = false
/* styles */
var submenu___vue_styles__ = null
/* scopeId */
var submenu___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var submenu___vue_module_identifier__ = null

var submenu_Component = normalizeComponent(
  submenu,
  submenu_render,
  submenu_staticRenderFns,
  submenu___vue_template_functional__,
  submenu___vue_styles__,
  submenu___vue_scopeId__,
  submenu___vue_module_identifier__
)

/* harmony default export */ var menu_submenu = (submenu_Component.exports);

// CONCATENATED MODULE: ./src/components/menu/index.js








// CONCATENATED MODULE: ./src/components/nav/index.js



var NavItem = {
  name: 'c-nav-item',
  functional: true,
  props: {
    title: {
      type: String,
      default: ''
    },
    active: {
      type: Boolean,
      default: false
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        children = _ref.children;

    var itemCls = ['nav__item'];
    var linkCls = ['nav__link'];

    var staticClass = data.staticClass,
        wrapperCls = data['class'],
        rest = objectWithoutProperties_default()(data, ['staticClass', 'class']);

    if (staticClass) {
      itemCls.push(staticClass);
    }

    if (props.active) {
      linkCls.push('is-active');
    }

    return h('li', {
      staticClass: itemCls,
      'class': wrapperCls
    }, [h('a', mergeData({
      staticClass: linkCls.join(' ')
    }, rest), [props.title]), children]);
  }
};


var Nav = {
  name: 'c-nav',
  functional: true,
  props: {
    type: {
      type: String
    },
    line: {
      type: String
    },
    size: {
      type: String
    }
  },
  render: function render(h, _ref2) {
    var props = _ref2.props,
        data = _ref2.data,
        children = _ref2.children;

    var cls = ['nav'];

    // type modifier
    if (props.type) {
      cls.push('nav--' + props.type);
    }

    // line modifier
    if (props.line) {
      cls.push('nav--line-' + props.line);
    }

    // size modifier
    if (props.size) {
      cls.push('nav--' + props.size);
    }

    return h('ul', mergeData({
      staticClass: cls.join(' ')
    }, data), children);
  }
};
// EXTERNAL MODULE: ./src/components/steps/style.scss
var steps_style = __webpack_require__("GXuI");
var steps_style_default = /*#__PURE__*/__webpack_require__.n(steps_style);

// CONCATENATED MODULE: ./src/components/steps/steps.js
/* harmony default export */ var steps = ({
  name: 'c-steps',
  props: {
    current: {
      type: [String, Number],
      default: 0
    },
    status: {
      type: String
    }
  },
  render: function render(h) {
    var _this = this;

    var activeIndex = parseFloat(this.current);
    var index = 1;
    return h(
      'ul',
      { 'class': 'steps' },
      [this.$slots.default.reduce(function (group, item) {
        if (item.componentOptions && item.componentOptions.Ctor.options.name === 'c-steps-item') {
          item.data['class'] = item.data['class'] = {};
          item.data['class']['is-active'] = index <= activeIndex;
          item.data['class']['is-error'] = index === activeIndex && _this.status === 'error';
          item.data['class']['is-warning'] = index === activeIndex && _this.status === 'warning';
          group.push(item);
          index++;
        }
        return group;
      }, [])]
    );
  }
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/steps/item.vue
//
//
//
//
//
//
//
//

/* harmony default export */ var item = ({
  name: 'c-steps-item',
  props: {
    title: {
      type: String
    },
    desc: {
      type: String
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-8ba9c1a4","hasScoped":false,"optionsId":"0","buble":{"transforms":{"stripWithFunctional":true}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/steps/item.vue
var item_render = function (_h,_vm) {var _c=_vm._c;return _c('li',{staticClass:"steps__item"},[_c('i',{staticClass:"steps__indicator"}),_vm._v(" "),_c('h4',{staticClass:"steps__title",domProps:{"textContent":_vm._s(_vm.title)}}),_vm._v(" "),(_vm.desc)?_c('p',{staticClass:"steps__desc",domProps:{"textContent":_vm._s(_vm.desc)}}):_vm._e()])}
var item_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/steps/item.vue
/* script */


/* template */

/* template functional */
var item___vue_template_functional__ = true
/* styles */
var item___vue_styles__ = null
/* scopeId */
var item___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var item___vue_module_identifier__ = null

var item_Component = normalizeComponent(
  item,
  item_render,
  item_staticRenderFns,
  item___vue_template_functional__,
  item___vue_styles__,
  item___vue_scopeId__,
  item___vue_module_identifier__
)

/* harmony default export */ var steps_item = (item_Component.exports);

// CONCATENATED MODULE: ./src/components/steps/index.js





// EXTERNAL MODULE: ./src/components/tabs/style.scss
var tabs_style = __webpack_require__("TfyY");
var tabs_style_default = /*#__PURE__*/__webpack_require__.n(tabs_style);

// CONCATENATED MODULE: ./src/components/tabs/tabs.js
/* harmony default export */ var tabs = ({
  name: 'c-tabs',

  props: {
    activeName: {
      type: String
    },
    type: {
      type: String,
      default: 'tabs'
    },
    size: {
      type: String
    }
  },

  data: function data() {
    return {
      currentName: '',
      panes: []
    };
  },


  watch: {
    activeName: function activeName(val) {
      this.currentName = val;
    }
  },

  mounted: function mounted() {
    this.currentName = this.activeName || this.panes[0].name;
  },


  methods: {
    handleTabClick: function handleTabClick(pane) {
      if (pane.disabled) return;
      this.currentName = pane.name;
    },
    addPane: function addPane(pane) {
      this.panes.push(pane);
    }
  },

  render: function render(h) {
    var _this = this;

    return h(
      'div',
      { 'class': 'tabs' },
      [h(
        'c-nav',
        { 'class': 'tabs__nav', attrs: { size: this.size, type: this.type || 'tabs' }
        },
        [this.panes.map(function (pane) {
          return h('c-nav-item', {
            attrs: {
              active: pane.isActive,
              disabled: pane.disabled,

              title: pane.label
            },
            on: {
              'click': function click(e) {
                return _this.handleTabClick(pane);
              }
            }
          });
        })]
      ), h(
        'div',
        { 'class': 'tabs__content' },
        [this.$slots.default]
      )]
    );
  }
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/tabs/tabs-pane.vue
//
//
//
//
//
//

/* harmony default export */ var tabs_pane = ({
  name: 'c-tabs-pane',

  props: {
    label: {
      type: String
    },
    name: {
      type: String
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },

  computed: {
    isActive: function isActive() {
      return this.$parent.currentName == this.name;
    }
  },

  created: function created() {
    this.$parent.addPane(this);
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-06792caa","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/tabs/tabs-pane.vue
var tabs_pane_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{directives:[{name:"show",rawName:"v-show",value:(_vm.isActive),expression:"isActive"}],staticClass:"tabs__pane",attrs:{"role":"tabpanel"}},[_vm._t("default")],2)}
var tabs_pane_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/tabs/tabs-pane.vue
/* script */


/* template */

/* template functional */
var tabs_pane___vue_template_functional__ = false
/* styles */
var tabs_pane___vue_styles__ = null
/* scopeId */
var tabs_pane___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var tabs_pane___vue_module_identifier__ = null

var tabs_pane_Component = normalizeComponent(
  tabs_pane,
  tabs_pane_render,
  tabs_pane_staticRenderFns,
  tabs_pane___vue_template_functional__,
  tabs_pane___vue_styles__,
  tabs_pane___vue_scopeId__,
  tabs_pane___vue_module_identifier__
)

/* harmony default export */ var tabs_tabs_pane = (tabs_pane_Component.exports);

// CONCATENATED MODULE: ./src/components/tabs/index.js





// EXTERNAL MODULE: ./src/components/pagination/style.scss
var pagination_style = __webpack_require__("5++G");
var pagination_style_default = /*#__PURE__*/__webpack_require__.n(pagination_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/pagination/pagination.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var pagination = ({
  name: "c-pagination",
  props: {
    value: {
      type: Number,
      default: 1
    },
    total: {
      type: Number,
      default: 1
    },
    range: {
      type: Number,
      default: 2
    },
    prev: {
      default: "Previous"
    },
    next: {
      default: "Next"
    },
    size: {
      type: String
    },
    type: {
      type: String
    },
    pager: {
      type: Boolean,
      default: true
    },
    jumper: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      current: 1
    };
  },

  watch: {
    value: {
      imediate: true,
      handler: function handler(val) {
        if (val !== this.current) {
          this.current = val;
        }
      }
    },
    current: function current(val) {
      this.$emit("input", val);
    }
  },
  computed: {
    chunks: function chunks() {
      var lower = this.current - this.range;
      var higher = this.current + this.range;

      if (lower <= 2) lower = 1;
      if (higher >= this.total - 1) higher = this.total;

      var result = [range(lower, higher)];

      if (lower > 2) result.unshift([1]);
      if (higher < this.total - 1) result.push([this.total]);

      return result;
    },
    prevDisabled: function prevDisabled() {
      return this.current === 1;
    },
    nextDisabled: function nextDisabled() {
      return this.current === this.total;
    },
    wrapperCls: function wrapperCls() {
      var cls = [];
      if (this.size) cls.push("pagination--" + this.size);
      if (this.type) cls.push("pagination--" + this.type);
      return cls;
    }
  },
  methods: {
    select: function select(paged) {
      this.current = paged;
    },
    selectPrev: function selectPrev() {
      if (this.current > 1) {
        this.select(this.current - 1);
      }
    },
    selectNext: function selectNext() {
      if (this.current < this.total) {
        this.select(this.current + 1);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-a1179414","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/pagination/pagination.vue
var pagination_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"pagination-container"},[_c('div',{staticClass:"pagination",class:_vm.wrapperCls},[(_vm.pager)?_c('a',{staticClass:"pagination__item",class:[_vm.prevDisabled && 'is-disabled'],attrs:{"role":"button"},domProps:{"textContent":_vm._s(_vm.prev)},on:{"click":_vm.selectPrev}}):_vm._e(),_vm._v(" "),_vm._l((_vm.chunks),function(chunk,i){return [_vm._l((chunk),function(paged,j){return [(_vm.current === paged)?_c('em',{key:i + '-' + j + 'e',staticClass:"pagination__item is-active",domProps:{"textContent":_vm._s(paged)}}):_c('a',{key:i + '-' + j + 'a',staticClass:"pagination__item",attrs:{"role":"button"},domProps:{"textContent":_vm._s(paged)},on:{"click":function($event){_vm.select(paged)}}})]}),_vm._v(" "),(i < _vm.chunks.length - 1)?_c('span',{key:i + 's',staticClass:"pagination__item is-disabled"},[_vm._v("...")]):_vm._e()]}),_vm._v(" "),(_vm.pager)?_c('a',{staticClass:"pagination__item",class:[_vm.nextDisabled && 'is-disabled'],attrs:{"role":"button"},domProps:{"textContent":_vm._s(_vm.next)},on:{"click":_vm.selectNext}}):_vm._e()],2),_vm._v(" "),(_vm.jumper)?_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.current),expression:"current"}],staticClass:"form-select pagination-jumper",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.current=$event.target.multiple ? $$selectedVal : $$selectedVal[0]}}},_vm._l((this.total),function(i){return _c('option',{key:i,domProps:{"value":i,"textContent":_vm._s(i)}})})):_vm._e()])}
var pagination_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/pagination/pagination.vue
/* script */


/* template */

/* template functional */
var pagination___vue_template_functional__ = false
/* styles */
var pagination___vue_styles__ = null
/* scopeId */
var pagination___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var pagination___vue_module_identifier__ = null

var pagination_Component = normalizeComponent(
  pagination,
  pagination_render,
  pagination_staticRenderFns,
  pagination___vue_template_functional__,
  pagination___vue_styles__,
  pagination___vue_scopeId__,
  pagination___vue_module_identifier__
)

/* harmony default export */ var pagination_pagination = (pagination_Component.exports);

// CONCATENATED MODULE: ./src/components/pagination/index.js



/* harmony default export */ var components_pagination = (pagination_pagination);
// EXTERNAL MODULE: ./src/components/flash/style.scss
var flash_style = __webpack_require__("lf+9");
var flash_style_default = /*#__PURE__*/__webpack_require__.n(flash_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/flash/flash.vue

//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var flash = ({
  name: 'c-flash',
  props: {
    type: {
      type: String,
      default: ''
    }
  },
  computed: {
    wrapperCls: function wrapperCls() {
      return defineProperty_default()({}, 'flash--' + this.type, !!this.type);
    }
  },
  methods: {
    close: function close() {
      this.$emit('close');
      this.$destroy();
    }
  },
  destroyed: function destroyed() {
    this.$el.parentNode.removeChild(this.$el);
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-4978fd4d","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/flash/flash.vue
var flash_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"flash",class:_vm.wrapperCls},[_c('a',{staticClass:"flash__close",attrs:{"role":"button"},on:{"click":_vm.close}},[_c('i',{staticClass:"i-remove"})]),_vm._v(" "),_c('div',{staticClass:"flash__content"},[_vm._t("default")],2)])}
var flash_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/flash/flash.vue
/* script */


/* template */

/* template functional */
var flash___vue_template_functional__ = false
/* styles */
var flash___vue_styles__ = null
/* scopeId */
var flash___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var flash___vue_module_identifier__ = null

var flash_Component = normalizeComponent(
  flash,
  flash_render,
  flash_staticRenderFns,
  flash___vue_template_functional__,
  flash___vue_styles__,
  flash___vue_scopeId__,
  flash___vue_module_identifier__
)

/* harmony default export */ var flash_flash = (flash_Component.exports);

// CONCATENATED MODULE: ./src/components/flash/index.js


// EXTERNAL MODULE: ./src/components/alert/style.scss
var alert_style = __webpack_require__("J74g");
var alert_style_default = /*#__PURE__*/__webpack_require__.n(alert_style);

// EXTERNAL MODULE: external {"root":"Vue","commonjs":"vue","commonjs2":"vue","amd":"vue"}
var external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue__ = __webpack_require__("lRwf");
var external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue___default = /*#__PURE__*/__webpack_require__.n(external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue__);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/alert/alert.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var alert_alert = ({
  name: "c-alert",
  props: {
    title: String,
    content: String,
    type: String,
    showCancel: {
      type: Boolean,
      default: true
    },
    cancelText: {
      type: String,
      default: "Cancel"
    },
    confirmText: {
      type: String,
      default: "Confirm"
    },
    onCancel: {
      type: Function,
      default: function _default() {}
    },
    onConfirm: {
      type: Function,
      default: function _default() {}
    },
    value: {
      type: Boolean,
      default: true
    }
  },
  data: function data() {
    return {
      visible: false
    };
  },

  computed: {
    icon: function icon() {
      switch (this.type) {
        case "info":
          return "alert";
          break;
        case "error":
          return "cross";
          break;
        case "success":
          return "check";
          break;
        case "warning":
          return "minus";
          break;
        default:
          return "";
          break;
      }
    }
  },
  methods: {
    show: function show() {
      this.visible = true;
    },
    hide: function hide() {
      this.visible = false;
    },
    handleHidden: function handleHidden() {
      this.$emit("hidden");
    },
    handleClickCancel: function handleClickCancel() {
      if (this.onCancel() !== false) {
        this.hide();
      }
    },
    handleClickConfirm: function handleClickConfirm() {
      if (this.onConfirm() !== false) {
        this.hide();
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-cfaf8c76","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/alert/alert.vue
var alert_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('c-modal',{attrs:{"className":['modal--alert', !!_vm.type ? ("is-" + _vm.type) : null],"closable":false},on:{"hidden":_vm.handleHidden},model:{value:(_vm.visible),callback:function ($$v) {_vm.visible=$$v},expression:"visible"}},[(_vm.title)?_c('h4',{staticClass:"modal__title",attrs:{"slot":"header"},slot:"header"},[(_vm.icon)?_c('c-svgicon',{attrs:{"name":_vm.icon,"size":"xs"}}):_vm._e(),_vm._v(_vm._s(_vm.title)+"\n  ")],1):_vm._e(),_vm._v("\n  "+_vm._s(_vm.content)+"\n  "),_c('div',{staticClass:"u-text-right",attrs:{"slot":"footer"},slot:"footer"},[(_vm.showCancel)?_c('c-button',{on:{"click":_vm.handleClickCancel}},[_vm._v(_vm._s(_vm.cancelText))]):_vm._e(),_vm._v(" "),_c('c-button',{attrs:{"type":"primary","smart":""},on:{"click":_vm.handleClickConfirm}},[_vm._v(_vm._s(_vm.confirmText))])],1)])}
var alert_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/alert/alert.vue
/* script */


/* template */

/* template functional */
var alert___vue_template_functional__ = false
/* styles */
var alert___vue_styles__ = null
/* scopeId */
var alert___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var alert___vue_module_identifier__ = null

var alert_Component = normalizeComponent(
  alert_alert,
  alert_render,
  alert_staticRenderFns,
  alert___vue_template_functional__,
  alert___vue_styles__,
  alert___vue_scopeId__,
  alert___vue_module_identifier__
)

/* harmony default export */ var components_alert_alert = (alert_Component.exports);

// CONCATENATED MODULE: ./src/components/alert/index.js



var AlertCtor = external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue___default.a.extend(components_alert_alert);
var noop = function noop() {};

var Alert = function Alert(_ref) {
  var _ref$title = _ref.title,
      title = _ref$title === undefined ? '' : _ref$title,
      _ref$content = _ref.content,
      content = _ref$content === undefined ? '' : _ref$content,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? '' : _ref$type,
      _ref$showCancel = _ref.showCancel,
      showCancel = _ref$showCancel === undefined ? false : _ref$showCancel,
      _ref$cancelText = _ref.cancelText,
      cancelText = _ref$cancelText === undefined ? 'Cancel' : _ref$cancelText,
      _ref$confirmText = _ref.confirmText,
      confirmText = _ref$confirmText === undefined ? 'Confirm' : _ref$confirmText,
      _ref$onConfirm = _ref.onConfirm,
      onConfirm = _ref$onConfirm === undefined ? noop : _ref$onConfirm,
      _ref$onCancel = _ref.onCancel,
      onCancel = _ref$onCancel === undefined ? noop : _ref$onCancel;

  var vm = new AlertCtor({
    propsData: {
      title: title,
      content: content,
      type: type,
      showCancel: showCancel,
      cancelText: cancelText,
      confirmText: confirmText,
      onConfirm: onConfirm,
      onCancel: onCancel
    },
    data: {
      visible: false
    }
  }).$mount();

  vm.$on('hidden', function () {
    vm.$destroy();
    document.body.removeChild(vm.$el);
  });

  document.body.appendChild(vm.$el);
  vm.$nextTick(function () {
    this.visible = true;
  });

  return vm;
};

/* harmony default export */ var components_alert = (Alert);
// EXTERNAL MODULE: ./src/components/popup/style.scss
var popup_style = __webpack_require__("efZv");
var popup_style_default = /*#__PURE__*/__webpack_require__.n(popup_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/popup/mask.vue
//
//
//
//
//
//
//
//

/* harmony default export */ var mask = ({
  name: "c-mask",

  data: function data() {
    return {
      visible: false
    };
  },
  mounted: function mounted() {
    document.body.appendChild(this.$el);
  },
  beforeDestroy: function beforeDestroy() {
    var parent = this.$el.parentNode;
    if (parent) {
      parent.removeChild(this.$el);
    }
  },


  methods: {
    show: function show() {
      this.visible = true;
    },
    hide: function hide() {
      this.visible = false;
    },
    toggle: function toggle() {
      this.visible = !this.visible;
    },
    onClick: function onClick(e) {
      this.$emit("click", e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-1623720c","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/popup/mask.vue
var mask_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":"fade","duration":300}},[(_vm.visible)?_c('div',{staticClass:"mask",on:{"click":_vm.onClick}}):_vm._e()])}
var mask_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/popup/mask.vue
/* script */


/* template */

/* template functional */
var mask___vue_template_functional__ = false
/* styles */
var mask___vue_styles__ = null
/* scopeId */
var mask___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var mask___vue_module_identifier__ = null

var mask_Component = normalizeComponent(
  mask,
  mask_render,
  mask_staticRenderFns,
  mask___vue_template_functional__,
  mask___vue_styles__,
  mask___vue_scopeId__,
  mask___vue_module_identifier__
)

/* harmony default export */ var popup_mask = (mask_Component.exports);

// CONCATENATED MODULE: ./src/components/popup/manager.js


var MaskCtor = external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue___default.a.extend(popup_mask);
var items = [];
var maskVM = null;

var showMask = function showMask() {
  if (!maskVM) {
    maskVM = new MaskCtor().$mount();
    maskVM.$on('click', function (e) {
      for (var i = items.length - 1; i >= 0; i--) {
        items[i].$emit('clickMask', e);
      }
    });
  }

  maskVM.show();
};

var hideMask = function hideMask() {
  if (!maskVM) return;

  maskVM.hide();
};

var manager = {
  register: function register(vm) {
    var index = items.indexOf(vm);
    if (index === -1) {
      items.push(vm);
      showMask();
      return items.length - 1;
    } else {
      return index;
    }
  },
  deregister: function deregister(vm) {
    var index = items.indexOf(vm);
    if (index !== -1) {
      items.splice(index, 1);
      if (items.length === 0) {
        hideMask();
      }
    }
  }
};

/* harmony default export */ var popup_manager = (manager);
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/popup/popup.vue

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var popup = ({
  name: "c-popup",

  props: {
    value: {
      type: Boolean,
      default: false
    },
    placement: {
      type: String,
      default: "center"
    },
    mask: {
      type: Boolean,
      default: true
    },
    closeOnClickMask: {
      type: Boolean,
      default: true
    },
    transition: {
      type: String,
      default: "fade"
    }
  },

  data: function data() {
    return {
      visible: false,
      isAnimating: false
    };
  },


  computed: {
    wrapperClass: function wrapperClass() {
      return defineProperty_default()({}, "is-" + this.placement, !!this.placement);
    }
  },

  watch: {
    value: function value(val) {
      if (val !== this.visible) {
        this.visible = val;
      }
    },
    visible: function visible(val) {
      this.$emit("input", val);
    }
  },

  mounted: function mounted() {
    document.body.appendChild(this.$el);
    this.$on("clickMask", function () {
      if (this.closeOnClickMask && this.visible) {
        this.hide();
      }
    });
  },
  beforeDestroy: function beforeDestroy() {
    popup_manager.deregister(this);
    document.body.removeChild(this.$el);
  },


  methods: {
    onBeforeEnter: function onBeforeEnter() {
      this.isAnimating = true;
      if (this.mask) {
        popup_manager.register(this);
      }
    },
    onAfterEnter: function onAfterEnter() {
      this.isAnimating = false;
    },
    onEnterCanceled: function onEnterCanceled() {
      this.isAnimating = false;
      popup_manager.deregister(this);
    },
    onBeforeLeave: function onBeforeLeave() {
      this.isAnimating = true;
      popup_manager.deregister(this);
    },
    onAfterLeave: function onAfterLeave() {
      this.isAnimating = false;
    },
    onLeaveCancelled: function onLeaveCancelled() {
      this.isAnimating = false;
      if (this.mask) {
        popup_manager.register(this);
      }
    },
    show: function show() {
      this.visible = true;
    },
    hide: function hide() {
      this.visible = false;
    },
    toggle: function toggle() {
      this.visible = !this.visible;
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-86d6bf26","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/popup/popup.vue
var popup_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.visible || _vm.isAnimating),expression:"visible || isAnimating"}],staticClass:"popup",class:_vm.wrapperClass},[_c('transition',{attrs:{"name":_vm.transition,"duration":400},on:{"beforeEnter":_vm.onBeforeEnter,"afterEnter":_vm.onAfterEnter,"enterCancelled":_vm.onEnterCanceled,"beforeLeave":_vm.onBeforeLeave,"afterLeave":_vm.onAfterLeave,"leaveCancelled":_vm.onLeaveCancelled}},[_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.visible),expression:"visible"}]},[_vm._t("default")],2)])],1)}
var popup_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/popup/popup.vue
/* script */


/* template */

/* template functional */
var popup___vue_template_functional__ = false
/* styles */
var popup___vue_styles__ = null
/* scopeId */
var popup___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var popup___vue_module_identifier__ = null

var popup_Component = normalizeComponent(
  popup,
  popup_render,
  popup_staticRenderFns,
  popup___vue_template_functional__,
  popup___vue_styles__,
  popup___vue_scopeId__,
  popup___vue_module_identifier__
)

/* harmony default export */ var popup_popup = (popup_Component.exports);

// CONCATENATED MODULE: ./src/components/popup/index.js



/* harmony default export */ var components_popup = (popup_popup);
// EXTERNAL MODULE: ./node_modules/babel-runtime/core-js/object/assign.js
var object_assign = __webpack_require__("woOf");
var assign_default = /*#__PURE__*/__webpack_require__.n(object_assign);

// EXTERNAL MODULE: ./node_modules/babel-runtime/helpers/typeof.js
var helpers_typeof = __webpack_require__("pFYg");
var typeof_default = /*#__PURE__*/__webpack_require__.n(helpers_typeof);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/modal/modal.vue



//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var modal = ({
  name: "c-modal",
  props: {
    value: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: ""
    },
    closable: {
      type: Boolean,
      default: true
    },
    size: {
      type: String,
      default: "md"
    },
    placement: {
      type: String,
      default: "center"
    },
    mask: {
      type: Boolean,
      default: true
    },
    closeOnClickMask: {
      type: Boolean,
      default: false
    },
    className: {
      type: [String, Array, Object],
      default: ""
    },
    transition: {
      type: String,
      default: function _default() {
        switch (this.placement) {
          case "right":
            return "slideleft";
            break;
          case "left":
            return "slideright";
            break;
          case "top":
            return "slidedown";
            break;
          case "center":
            return "zoom";
            break;
          default:
            return "fade";
            break;
        }
      }
    }
  },
  data: function data() {
    return {
      visible: this.value
    };
  },

  computed: {
    currentPlacement: function currentPlacement() {
      if (["top", "left", "right", "center"].indexOf(this.placement) !== -1) {
        return this.placement;
      } else {
        return "center";
      }
    },
    modalClass: function modalClass() {
      var cls = defineProperty_default()({}, "modal--" + this.currentPlacement, true);
      var parentClass = this.className;

      if (this.size) {
        cls["modal--" + this.size] = true;
      }

      if (typeof parentClass === "string") {
        if (parentClass) {
          cls[parentClass] = true;
        }
      } else if (Array.isArray(parentClass)) {
        for (var i = 0, len = parentClass.length; i < len; i++) {
          if (parentClass[i]) {
            cls[parentClass[i]] = true;
          }
        }
      } else if ((typeof parentClass === "undefined" ? "undefined" : typeof_default()(parentClass)) === "object") {
        assign_default()(cls, parentClass);
      }

      return cls;
    }
  },
  watch: {
    value: function value(val) {
      if (val !== this.visible) {
        this.visible = val;
      }
    }
  },
  methods: {
    handleClickMask: function handleClickMask(e) {
      if (this.closeOnClickMask && this.visible) {
        this.close();
      }
      this.$emit("clickMask", e);
    },
    close: function close() {
      this.visible = false;
      this.$emit("input", false);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-50471849","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/modal/modal.vue
var modal_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('c-popup',{staticClass:"modal-popup",attrs:{"value":_vm.visible,"transition":_vm.transition,"mask":_vm.mask,"closeOnClickMask":_vm.closeOnClickMask,"placement":_vm.currentPlacement},on:{"shown":function($event){_vm.$emit('shown')},"hidden":function($event){_vm.$emit('hidden')},"clickMask":_vm.handleClickMask}},[_c('section',{staticClass:"modal",class:_vm.modalClass,attrs:{"role":"dialog"}},[(_vm.$slots.header || _vm.title || _vm.closable)?_c('header',{staticClass:"modal__header"},[_vm._t("header"),_vm._v(" "),(!_vm.$slots.header && _vm.title)?_c('h4',{staticClass:"modal__title",domProps:{"innerHTML":_vm._s(_vm.title)}}):_vm._e(),_vm._v(" "),(_vm.closable)?_c('a',{staticClass:"modal__close",attrs:{"role":"button"},on:{"click":_vm.close}},[_c('i',{staticClass:"i-remove"})]):_vm._e()],2):_vm._e(),_vm._v(" "),_c('div',{staticClass:"modal__body"},[_vm._t("default")],2),_vm._v(" "),(_vm.$slots.footer)?_c('footer',{staticClass:"modal__footer"},[_vm._t("footer")],2):_vm._e()])])}
var modal_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/modal/modal.vue
/* script */


/* template */

/* template functional */
var modal___vue_template_functional__ = false
/* styles */
var modal___vue_styles__ = null
/* scopeId */
var modal___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var modal___vue_module_identifier__ = null

var modal_Component = normalizeComponent(
  modal,
  modal_render,
  modal_staticRenderFns,
  modal___vue_template_functional__,
  modal___vue_styles__,
  modal___vue_scopeId__,
  modal___vue_module_identifier__
)

/* harmony default export */ var modal_modal = (modal_Component.exports);

// CONCATENATED MODULE: ./src/components/modal/index.js


/* harmony default export */ var components_modal = (modal_modal);
// EXTERNAL MODULE: ./src/components/toast/style.scss
var toast_style = __webpack_require__("Dd5s");
var toast_style_default = /*#__PURE__*/__webpack_require__.n(toast_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/toast/toast.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var toast = ({
  name: "c-toast",

  props: {
    icon: {
      type: String
    },
    content: {
      type: String
    },
    locked: {
      type: Boolean,
      default: false
    }
  },

  data: function data() {
    return {
      visible: true
    };
  },


  methods: {
    handleAfterLeave: function handleAfterLeave() {
      this.$emit("hidden");
    },
    show: function show() {
      this.visible = true;
    },
    hide: function hide() {
      this.visible = false;
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-0d6cea13","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/toast/toast.vue
var toast_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"toast",class:{'is-locked': _vm.locked}},[_c('transition',{attrs:{"name":"fade","appear":""},on:{"afterLeave":_vm.handleAfterLeave}},[_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.visible),expression:"visible"}],staticClass:"toast__content"},[(_vm.icon)?[(_vm.icon === 'spin')?_c('i',{staticClass:"toast__spin"}):_c('c-svgicon',{attrs:{"size":"48","name":_vm.icon}})]:_vm._e(),_vm._v(" "),(_vm.$slots.default || _vm.content)?_c('div',{staticClass:"toast__text"},[_vm._v("\n      "+_vm._s(this.content)+"\n      "),_vm._t("default")],2):_vm._e()],2)])],1)}
var toast_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/toast/toast.vue
/* script */


/* template */

/* template functional */
var toast___vue_template_functional__ = false
/* styles */
var toast___vue_styles__ = null
/* scopeId */
var toast___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var toast___vue_module_identifier__ = null

var toast_Component = normalizeComponent(
  toast,
  toast_render,
  toast_staticRenderFns,
  toast___vue_template_functional__,
  toast___vue_styles__,
  toast___vue_scopeId__,
  toast___vue_module_identifier__
)

/* harmony default export */ var toast_toast = (toast_Component.exports);

// CONCATENATED MODULE: ./src/components/toast/index.js



var ToastCtor = external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue___default.a.extend(toast_toast);

var generate = function generate(_ref) {
  var _ref$icon = _ref.icon,
      icon = _ref$icon === undefined ? '' : _ref$icon,
      _ref$content = _ref.content,
      content = _ref$content === undefined ? '' : _ref$content,
      _ref$locked = _ref.locked,
      locked = _ref$locked === undefined ? false : _ref$locked,
      _ref$duration = _ref.duration,
      duration = _ref$duration === undefined ? 2000 : _ref$duration;

  var timeout = void 0;
  var vm = new ToastCtor({
    propsData: {
      icon: icon,
      content: content,
      locked: locked
    }
  }).$mount();

  if (duration > 0) {
    timeout = setTimeout(function () {
      vm.hide();
    }, duration);
  }

  vm.$on('hidden', function () {
    vm.$destroy();
    document.body.removeChild(vm.$el);
  });

  document.body.appendChild(vm.$el);

  return vm;
};

toast_toast.info = function () {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;

  return generate({
    icon: '',
    content: content,
    duration: duration
  });
};

toast_toast.succeed = function () {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;

  return generate({
    icon: 'check',
    content: content,
    duration: duration
  });
};

toast_toast.failed = function () {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;

  return generate({
    icon: 'cross',
    content: content,
    duration: duration
  });
};

toast_toast.warn = function () {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;

  return generate({
    icon: 'alert',
    content: content,
    duration: duration
  });
};

toast_toast.loading = function () {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

  return generate({
    icon: 'spin',
    content: content,
    duration: duration,
    locked: true
  });
};

/* harmony default export */ var components_toast = (toast_toast);
// EXTERNAL MODULE: ./src/components/notification/style.scss
var notification_style = __webpack_require__("tV5f");
var notification_style_default = /*#__PURE__*/__webpack_require__.n(notification_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/notification/notification.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var notification = ({
  name: 'c-notification',
  props: {
    title: {
      type: String,
      default: ''
    },
    type: {
      type: String,
      default: ''
    },
    closeable: {
      type: Boolean,
      default: true
    },
    duration: {
      type: Number,
      default: 1000
    }
  },
  mounted: function mounted() {
    if (this.duration > 0) {
      this.timer = setTimeout(this.close, this.duration);
    }
  },

  methods: {
    close: function close() {
      if (this.duration) {
        clearTimeout(this.timer);
      }
      this.$emit('close');
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-9c1259ec","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/notification/notification.vue
var notification_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"notification"},[(_vm.closeable)?_c('a',{staticClass:"notification__close",attrs:{"role":"button"},on:{"click":_vm.close}},[_c('i',{staticClass:"i-remove"})]):_vm._e(),_vm._v(" "),(_vm.type)?_c('div',{staticClass:"notification__media"},[(_vm.type === 'danger')?_c('c-svgicon',{attrs:{"size":"36","name":"cross","fill":"#f44336"}}):(_vm.type === 'success')?_c('c-svgicon',{attrs:{"size":"36","name":"check","fill":"#4caf50"}}):(_vm.type === 'info')?_c('c-svgicon',{attrs:{"size":"36","name":"alert","fill":"#2196f3"}}):_c('i',{class:_vm.type})],1):_vm._e(),_vm._v(" "),_c('div',{staticClass:"notification__content"},[_c('div',{staticClass:"notification__title"},[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),_c('div',{staticClass:"notification__text"},[_vm._t("default")],2)])])}
var notification_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/notification/notification.vue
/* script */


/* template */

/* template functional */
var notification___vue_template_functional__ = false
/* styles */
var notification___vue_styles__ = null
/* scopeId */
var notification___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var notification___vue_module_identifier__ = null

var notification_Component = normalizeComponent(
  notification,
  notification_render,
  notification_staticRenderFns,
  notification___vue_template_functional__,
  notification___vue_styles__,
  notification___vue_scopeId__,
  notification___vue_module_identifier__
)

/* harmony default export */ var notification_notification = (notification_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/notification/notification-list.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var notification_list = ({
  name: 'c-notification-list',

  components: {
    'c-notification': notification_notification
  },

  data: function data() {
    return {
      items: []
    };
  },


  methods: {
    remove: function remove(index) {
      this.items.splice(index, 1);
    },
    add: function add(obj) {
      this.items.unshift(obj);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-09958385","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/notification/notification-list.vue
var notification_list_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition-group',{staticClass:"notification-list",attrs:{"tag":"div","name":"is"}},_vm._l((_vm.items),function(item,i){return _c('c-notification',{key:item.id,attrs:{"type":item.type,"title":item.title,"closeable":item.closeable,"duration":item.duration},on:{"close":function($event){_vm.remove(i)}}},[_vm._v("\n    "+_vm._s(item.content)+"\n  ")])}))}
var notification_list_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/notification/notification-list.vue
/* script */


/* template */

/* template functional */
var notification_list___vue_template_functional__ = false
/* styles */
var notification_list___vue_styles__ = null
/* scopeId */
var notification_list___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var notification_list___vue_module_identifier__ = null

var notification_list_Component = normalizeComponent(
  notification_list,
  notification_list_render,
  notification_list_staticRenderFns,
  notification_list___vue_template_functional__,
  notification_list___vue_styles__,
  notification_list___vue_scopeId__,
  notification_list___vue_module_identifier__
)

/* harmony default export */ var notification_notification_list = (notification_list_Component.exports);

// CONCATENATED MODULE: ./src/components/notification/index.js







var NotificationListCtor = external___root___Vue___commonjs___vue___commonjs2___vue___amd___vue___default.a.extend(notification_notification_list);
var notification_DEFAULT_OPTIONS = {
  id: 0,
  title: '',
  content: '',
  type: '',
  duration: 5000,
  closeable: false
};
var vm = void 0,
    seed = 0;

function notify(options) {
  seed++;

  if (typeof vm === 'undefined') {
    vm = new NotificationListCtor().$mount();
    document.body.appendChild(vm.$el);
  }

  if (typeof options === 'string') {
    options = extends_default()({}, notification_DEFAULT_OPTIONS, {
      id: seed,
      title: options
    });
  } else if ((typeof options === 'undefined' ? 'undefined' : typeof_default()(options)) === 'object') {
    options = extends_default()({}, notification_DEFAULT_OPTIONS, options, {
      id: seed
    });
  }

  vm.add(options);
}


// CONCATENATED MODULE: ./src/components/form/form.js



/* harmony default export */ var form_form = ({
  name: 'c-form',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'form'
    },
    layout: {
      type: String
    },
    span: {
      type: [String, Number],
      default: 220
    }
  },
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;

    var wrapperClass = {
      'form-inline': props.layout === 'inline',
      'form-horizontal': props.layout === 'horizontal'
    };

    var children = [];

    // horizontal layout
    if (props.layout === 'horizontal') {
      var labelWidth = parseFloat(props.span);
      children = slots().default.reduce(function (fieldset, field) {
        if (field.fnOptions && field.fnOptions.name === 'c-form-field') {
          var labelChild = void 0;
          var contentChildren = field.children.reduce(function (a, b) {
            if (b.data && b.data['class'] === 'form-label') {
              labelChild = b;
            } else {
              a.push(b);
            }
            return a;
          }, []);

          fieldset.push(h(
            'div',
            babel_helper_vue_jsx_merge_props_default()([{ 'class': 'form-field' }, field.data]),
            [h(
              'div',
              { 'class': 'l-row' },
              [h(
                'div',
                { 'class': 'form-field__start', style: { width: labelWidth + 'px' } },
                [labelChild]
              ), h(
                'div',
                { 'class': 'l-col@md' },
                [contentChildren]
              )]
            )]
          ));
        } else {
          fieldset.push(field);
        }
        return fieldset;
      }, []);
    } else {
      children = slots().default;
    }

    return h(props.tag, mergeData({
      'class': wrapperClass
    }, data), children);
  }
});
// CONCATENATED MODULE: ./src/components/form/addon.js

/* harmony default export */ var addon = ({
  name: 'c-form-addon',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        slots = _ref.slots;

    return h(
      "span",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "form-addon" }, data]),
      [slots().default]
    );
  }
});
// CONCATENATED MODULE: ./src/components/form/group.js

/* harmony default export */ var group = ({
  name: 'c-form-group',
  functional: true,
  render: function render(h, _ref) {
    var data = _ref.data,
        slots = _ref.slots;

    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "form-group" }, data]),
      [slots().default]
    );
  }
});
// CONCATENATED MODULE: ./src/components/form/field.js

/* harmony default export */ var form_field = ({
  name: 'c-form-field',
  functional: true,
  render: function render(h, _ref) {
    var props = _ref.props,
        data = _ref.data,
        slots = _ref.slots;
    var label = props.label,
        help = props.help;


    return h(
      "div",
      babel_helper_vue_jsx_merge_props_default()([{ "class": "form-field" }, data]),
      [label ? h("label", { "class": "form-label", domProps: {
          "innerHTML": label
        }
      }) : null, slots().default, help ? h(
        "small",
        { "class": "form-help" },
        [help]
      ) : null]
    );
  }
});
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/input.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var input = ({
  name: "c-form-input",
  inheritAttrs: false,
  props: {
    value: {
      default: ""
    },
    status: {
      type: String,
      default: ""
    },
    clearable: {
      type: Boolean,
      default: false
    },
    flat: {
      type: Boolean,
      default: false
    },
    iconStart: {
      type: String,
      default: ""
    },
    iconEnd: {
      type: String,
      default: ""
    },
    addonStart: {
      type: String,
      default: ""
    },
    addonEnd: {
      type: String,
      default: ""
    },
    size: {
      type: String,
      default: ""
    }
  },
  data: function data() {
    return {
      currentValue: this.value
    };
  },

  watch: {
    value: function value(val) {
      if (val !== this.currentValue) {
        this.currentValue = val;
      }
    }
  },
  methods: {
    clear: function clear() {
      this.currentValue = "";
      this.$emit("input", this.currentValue);
    },
    handleInput: function handleInput(e) {
      this.currentValue = e.target.value;
      this.$emit("input", this.currentValue, e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-095875be","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/input.vue
var input_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.addonStart || _vm.addonEnd)?_c('div',{staticClass:"form-group"},[(_vm.addonStart)?_c('span',{staticClass:"form-addon",domProps:{"innerHTML":_vm._s(_vm.addonStart)}}):_vm._e(),_vm._v(" "),_c('div',{staticClass:"form-control",class:{
      'has-clear': _vm.clearable,
      'has-icon-start': _vm.iconStart,
      'has-icon-end': _vm.iconEnd
    }},[(_vm.iconStart)?_c('i',{staticClass:"form-control__icon-start",class:[_vm.iconStart]}):_vm._e(),_vm._v(" "),_c('input',_vm._b({staticClass:"form-input",class:( _obj = {
        'is-flat': _vm.flat
      }, _obj[("form-input--" + _vm.size)] = !!_vm.size, _obj[("is-" + _vm.status)] = !!_vm.status, _obj ),domProps:{"value":_vm.currentValue},on:{"input":_vm.handleInput,"blur":function($event){_vm.$emit('blur', $event)},"focus":function($event){_vm.$emit('focus', $event)},"change":function($event){_vm.$emit('change', $event)}}},'input',_vm.$attrs,false)),_vm._v(" "),(_vm.iconEnd)?_c('i',{staticClass:"form-control__icon-end",class:[_vm.iconEnd]}):_vm._e(),_vm._v(" "),(_vm.clearable)?_c('a',{staticClass:"form-control__clear",attrs:{"role":"button"},on:{"click":_vm.clear}},[_vm._v("×")]):_vm._e()]),_vm._v(" "),(_vm.addonEnd)?_c('span',{staticClass:"form-addon",domProps:{"innerHTML":_vm._s(_vm.addonEnd)}}):_vm._e()]):_c('div',{staticClass:"form-control",class:{
    'has-clear': _vm.clearable,
    'has-icon-start': _vm.iconStart,
    'has-icon-end': _vm.iconEnd
  }},[(_vm.iconStart)?_c('i',{staticClass:"form-control__icon-start",class:[_vm.iconStart]}):_vm._e(),_vm._v(" "),_c('input',_vm._b({staticClass:"form-input",class:( _obj$1 = {
      'is-flat': _vm.flat
    }, _obj$1[("form-input--" + _vm.size)] = !!_vm.size, _obj$1[("is-" + _vm.status)] = !!_vm.status, _obj$1 ),domProps:{"value":_vm.currentValue},on:{"input":_vm.handleInput,"blur":function($event){_vm.$emit('blur', $event)},"focus":function($event){_vm.$emit('focus', $event)},"change":function($event){_vm.$emit('change', $event)}}},'input',_vm.$attrs,false)),_vm._v(" "),(_vm.iconEnd)?_c('i',{staticClass:"form-control__icon-end",class:[_vm.iconEnd]}):_vm._e(),_vm._v(" "),(_vm.clearable)?_c('a',{staticClass:"form-control__clear",attrs:{"role":"button"},on:{"click":_vm.clear}},[_vm._v("×")]):_vm._e()])
var _obj;
var _obj$1;}
var input_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/input.vue
/* script */


/* template */

/* template functional */
var input___vue_template_functional__ = false
/* styles */
var input___vue_styles__ = null
/* scopeId */
var input___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var input___vue_module_identifier__ = null

var input_Component = normalizeComponent(
  input,
  input_render,
  input_staticRenderFns,
  input___vue_template_functional__,
  input___vue_styles__,
  input___vue_scopeId__,
  input___vue_module_identifier__
)

/* harmony default export */ var form_input = (input_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/radio.vue
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var form_radio = ({
  name: "c-form-radio",
  inheritAttrs: false,
  model: {
    prop: "checked",
    event: "sync"
  },
  props: {
    label: {
      type: String,
      default: ""
    },
    checked: {
      type: String,
      default: ""
    },
    value: {
      type: String
    }
  },
  methods: {
    handleChange: function handleChange(e) {
      if (e.target.checked) {
        this.$emit("sync", e.target.value);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-3589f29e","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/radio.vue
var radio_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"form-radio"},[_c('input',_vm._b({attrs:{"type":"radio"},domProps:{"value":_vm.value,"checked":_vm.checked === _vm.value},on:{"change":_vm.handleChange}},'input',_vm.$attrs,false)),_vm._v(" "),_c('span',{staticClass:"form-radio__indicator"}),_vm._v("\n  "+_vm._s(_vm.label)+"\n")])}
var radio_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/radio.vue
/* script */


/* template */

/* template functional */
var radio___vue_template_functional__ = false
/* styles */
var radio___vue_styles__ = null
/* scopeId */
var radio___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var radio___vue_module_identifier__ = null

var radio_Component = normalizeComponent(
  form_radio,
  radio_render,
  radio_staticRenderFns,
  radio___vue_template_functional__,
  radio___vue_styles__,
  radio___vue_scopeId__,
  radio___vue_module_identifier__
)

/* harmony default export */ var components_form_radio = (radio_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/checkbox.vue
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var form_checkbox = ({
  name: "c-form-checkbox",
  inheritAttrs: false,
  model: {
    prop: "checked",
    event: "sync"
  },
  props: {
    label: {
      type: String,
      default: ""
    },
    checked: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    onChange: function onChange(e) {
      this.$emit("sync", e.target.checked);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-523a9080","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/checkbox.vue
var checkbox_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"form-checkbox"},[_c('input',_vm._b({attrs:{"type":"checkbox"},domProps:{"checked":_vm.checked},on:{"change":_vm.onChange}},'input',_vm.$attrs,false)),_vm._v(" "),_c('span',{staticClass:"form-checkbox__indicator"}),_vm._v("\n  "+_vm._s(_vm.label)+"\n")])}
var checkbox_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/checkbox.vue
/* script */


/* template */

/* template functional */
var checkbox___vue_template_functional__ = false
/* styles */
var checkbox___vue_styles__ = null
/* scopeId */
var checkbox___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var checkbox___vue_module_identifier__ = null

var checkbox_Component = normalizeComponent(
  form_checkbox,
  checkbox_render,
  checkbox_staticRenderFns,
  checkbox___vue_template_functional__,
  checkbox___vue_styles__,
  checkbox___vue_scopeId__,
  checkbox___vue_module_identifier__
)

/* harmony default export */ var components_form_checkbox = (checkbox_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/switch.vue
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var form_switch = ({
  name: "c-form-switch",
  inheritAttrs: false,
  model: {
    prop: "checked",
    event: "sync"
  },
  props: {
    label: {
      type: String,
      default: ""
    },
    checked: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    handleChange: function handleChange(e) {
      this.$emit("sync", e.target.checked);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-17188d60","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/switch.vue
var switch_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"form-switch"},[_c('input',_vm._b({attrs:{"type":"checkbox"},domProps:{"checked":_vm.checked},on:{"change":_vm.handleChange}},'input',_vm.$attrs,false)),_vm._v(" "),_c('span',{staticClass:"form-switch__indicator"}),_vm._v("\n  "+_vm._s(_vm.label)+"\n")])}
var switch_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/switch.vue
/* script */


/* template */

/* template functional */
var switch___vue_template_functional__ = false
/* styles */
var switch___vue_styles__ = null
/* scopeId */
var switch___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var switch___vue_module_identifier__ = null

var switch_Component = normalizeComponent(
  form_switch,
  switch_render,
  switch_staticRenderFns,
  switch___vue_template_functional__,
  switch___vue_styles__,
  switch___vue_scopeId__,
  switch___vue_module_identifier__
)

/* harmony default export */ var components_form_switch = (switch_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/select.vue

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var getSelectValues = function getSelectValues(select) {
  var result = [];
  var options = select.options;
  for (var i = 0, len = options.length; i < len; i++) {
    if (options[i].selected) {
      result.push(options[i].value || options[i].text);
    }
  }

  return result;
};

var setSelectValues = function setSelectValues(select, values) {
  var options = select.options;
  for (var i = 0, len = options.length; i < len; i++) {
    if (values.indexOf(options[i].value) !== -1) {
      options[i].selected = true;
    } else {
      options[i].selected = false;
    }
  }
};

/* harmony default export */ var form_select = ({
  name: "c-form-select",
  props: {
    value: {
      type: [String, Array]
    },
    options: {
      type: Array,
      default: function _default() {
        return {};
      }
    },
    multiple: {
      type: Boolean,
      default: false
    },
    status: {
      type: String
    },
    size: {
      type: String
    }
  },
  data: function data() {
    return {
      selected: null
    };
  },

  computed: {
    wrapperCls: function wrapperCls() {
      var _ref;

      return _ref = {}, defineProperty_default()(_ref, "form-input--" + this.size, !!this.size), defineProperty_default()(_ref, "is-" + this.status, !!this.status), _ref;
    }
  },
  watch: {
    value: function value(val) {
      if (val !== this.selected) {
        this.updateSelect();
      }
    }
  },
  mounted: function mounted() {
    this.updateSelect();
  },

  methods: {
    updateSelect: function updateSelect() {
      if (Array.isArray(this.value)) {
        setSelectValues(this.$el, this.value);
      } else {
        this.$el.value = this.value;
      }
    },
    handleChange: function handleChange(e) {
      if (this.multiple) {
        this.selected = getSelectValues(this.$el);
      } else {
        this.selected = e.target.value;
      }
      this.$emit("input", this.selected);
      this.$emit("change", e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2a3cb3b8","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/select.vue
var select_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('select',{staticClass:"form-select",class:_vm.wrapperCls,attrs:{"multiple":_vm.multiple},on:{"change":_vm.handleChange,"focus":function($event){_vm.$emit('focus', $event)},"blur":function($event){_vm.$emit('blur', $event)}}},_vm._l((_vm.options),function(item){return _c('option',{key:item.value,domProps:{"value":item.value,"textContent":_vm._s(item.label)}})}))}
var select_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/select.vue
/* script */


/* template */

/* template functional */
var select___vue_template_functional__ = false
/* styles */
var select___vue_styles__ = null
/* scopeId */
var select___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var select___vue_module_identifier__ = null

var select_Component = normalizeComponent(
  form_select,
  select_render,
  select_staticRenderFns,
  select___vue_template_functional__,
  select___vue_styles__,
  select___vue_scopeId__,
  select___vue_module_identifier__
)

/* harmony default export */ var components_form_select = (select_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/textarea.vue
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var form_textarea = ({
  name: "c-form-textarea",
  props: {
    value: {
      default: ""
    },
    status: {
      type: String
    },
    flat: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    handleInput: function handleInput(e) {
      this.$emit("input", e.target.value, e);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-0b1a7867","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/textarea.vue
var textarea_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('textarea',{staticClass:"form-textarea",class:( _obj = {
    'is-flat': _vm.flat
  }, _obj[("is-" + _vm.status)] = !!_vm.status, _obj ),domProps:{"value":_vm.value},on:{"input":_vm.handleInput,"blur":function($event){_vm.$emit('input', $event)},"focus":function($event){_vm.$emit('focus', $event)}}})
var _obj;}
var textarea_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/textarea.vue
/* script */


/* template */

/* template functional */
var textarea___vue_template_functional__ = false
/* styles */
var textarea___vue_styles__ = null
/* scopeId */
var textarea___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var textarea___vue_module_identifier__ = null

var textarea_Component = normalizeComponent(
  form_textarea,
  textarea_render,
  textarea_staticRenderFns,
  textarea___vue_template_functional__,
  textarea___vue_styles__,
  textarea___vue_scopeId__,
  textarea___vue_module_identifier__
)

/* harmony default export */ var components_form_textarea = (textarea_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/form/counter.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var counter = ({
  name: 'c-form-counter',
  props: {
    value: {
      type: Number,
      default: 0
    },
    step: {
      type: Number,
      default: 1,
      validator: function validator(val) {
        return val > 0;
      }
    },
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number
    },
    editable: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      count: 0
    };
  },

  watch: {
    value: function value(newVal) {
      if (newVal !== this.count) {
        this.count = newVal;
      }
    },
    count: function count(newVal, oldVal) {
      if (this.max && newVal >= this.max) {
        this.count = this.max;
      } else if (this.min && newVal <= this.min) {
        this.count = this.min;
      }

      if (newVal !== oldVal) {
        this.$emit('input', newVal);
      }
    }
  },
  methods: {
    increase: function increase() {
      if (this.disabled) return;

      if (!this.max || this.count < this.max) {
        this.count += this.step;
      }
    },
    decrease: function decrease() {
      if (this.disabled) return;

      if (!this.min || this.count > this.min) {
        this.count -= this.step;
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-154b85e4","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/form/counter.vue
var counter_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group form-counter"},[_c('button',{staticClass:"form-addon form-counter__btn",attrs:{"disabled":_vm.disabled || (_vm.min && _vm.count <= _vm.min)},on:{"click":_vm.decrease}},[_vm._v("-")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model.number",value:(_vm.count),expression:"count",modifiers:{"number":true}}],staticClass:"form-input form-counter__input",attrs:{"disabled":_vm.disabled,"readonly":!_vm.editable},domProps:{"value":(_vm.count)},on:{"input":function($event){if($event.target.composing){ return; }_vm.count=_vm._n($event.target.value)},"blur":function($event){_vm.$forceUpdate()}}}),_vm._v(" "),_c('button',{staticClass:"form-addon form-counter__btn",attrs:{"disabled":_vm.disabled || (_vm.max && _vm.count >= _vm.max)},on:{"click":_vm.increase}},[_vm._v("+")])])}
var counter_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/form/counter.vue
/* script */


/* template */

/* template functional */
var counter___vue_template_functional__ = false
/* styles */
var counter___vue_styles__ = null
/* scopeId */
var counter___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var counter___vue_module_identifier__ = null

var counter_Component = normalizeComponent(
  counter,
  counter_render,
  counter_staticRenderFns,
  counter___vue_template_functional__,
  counter___vue_styles__,
  counter___vue_scopeId__,
  counter___vue_module_identifier__
)

/* harmony default export */ var form_counter = (counter_Component.exports);

// CONCATENATED MODULE: ./src/components/form/index.js













// EXTERNAL MODULE: ./src/components/slider/style.scss
var slider_style = __webpack_require__("PClm");
var slider_style_default = /*#__PURE__*/__webpack_require__.n(slider_style);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/slider/slider.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var startDragMousePos = 0,
    startVal = 0;
/* harmony default export */ var slider = ({
  name: 'c-slider',

  props: {
    value: {
      type: [Array, Number],
      default: 0
    },
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number,
      default: 100
    },
    step: {
      type: Number,
      default: 1
    },
    dots: {
      type: Boolean,
      default: false
    },
    range: {
      type: Boolean,
      default: false
    },
    color: {
      type: String,
      default: '#1BB934'
    },
    format: {
      type: Function,
      default: function _default(val) {
        return val;
      }
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },

  data: function data() {
    return {
      isDragging: false,
      isDragingUpper: false,
      values: [this.min, this.max]
    };
  },


  watch: {
    value: {
      immediate: true,
      handler: function handler(val) {
        if (Array.isArray(val) && (val[0] !== this.values[0] || val[1] !== this.values[1]) || val !== this.values[0]) {
          this.updateValue(val);
        }
      }
    },
    disabled: function disabled(newVal) {
      if (!newVal) {
        this.stopDrag();
      }
    }
  },

  computed: {
    lowerHandlePosition: function lowerHandlePosition() {
      return (this.values[0] - this.min) / (this.max - this.min) * 100;
    },
    upperHandlePosition: function upperHandlePosition() {
      return (this.values[1] - this.min) / (this.max - this.min) * 100;
    },
    barStyle: function barStyle() {
      if (this.range) {
        return {
          'width': (this.values[1] - this.values[0]) / (this.max - this.min) * 100 + '%',
          'left': this.lowerHandlePosition + '%',
          'background-color': this.color
        };
      } else {
        return {
          'width': this.values[0] / (this.max - this.min) * 100 + '%',
          'background-color': this.color
        };
      }
    },
    stops: function stops() {
      var total = Math.floor((this.max - this.min) / this.step);
      var result = [];
      var stepGutter = 100 * this.step / (this.max - this.min);
      for (var i = 1; i <= total; i++) {
        result.push(i * stepGutter);
      }
      return result;
    }
  },

  methods: {
    updateValue: function updateValue(newVal) {
      var newValues = [];

      if (Array.isArray(newVal)) {
        newValues = [newVal[0], newVal[1]];
      } else {
        newValues[0] = newVal;
      }

      if (typeof newValues[0] !== 'number') {
        newValues[0] = this.values[0];
      } else {
        newValues[0] = Math.round((newValues[0] - this.min) / this.step) * this.step + this.min;
      }

      if (typeof newValues[1] !== 'number') {
        newValues[1] = this.values[1];
      } else {
        newValues[1] = Math.round((newValues[1] - this.min) / this.step) * this.step + this.min;
      }

      // value boundary adjust
      if (newValues[0] < this.min) {
        newValues[0] = this.min;
      }
      if (newValues[1] > this.max) {
        newValues[1] = this.max;
      }
      if (newValues[0] > newValues[1]) {
        if (newValues[0] === this.values[0]) {
          newValues[1] = newValues[0];
        } else {
          newValues[0] = newValues[1];
        }
      }

      if (this.values[0] === newValues[0] && this.values[1] === newValues[1]) return;

      this.values = newValues;

      if (this.range) {
        this.$emit('input', this.values);
      } else {
        this.$emit('input', this.values[0]);
      }
    },
    startLowerDrag: function startLowerDrag(e) {
      if (this.disabled) return;
      e.preventDefault();
      e.stopPropagation();
      e = e.changedTouches ? e.changedTouches[0] : e;
      startDragMousePos = e.pageX;
      startVal = this.values[0];
      this.isDragingUpper = false;
      this.isDragging = true;
      window.addEventListener('mousemove', this.onDrag);
      window.addEventListener('touchmove', this.onDrag);
      window.addEventListener('mouseup', this.onUp);
      window.addEventListener('touchend', this.onUp);
    },
    startUpperDrag: function startUpperDrag(e) {
      if (this.disabled) return;
      e.preventDefault();
      e.stopPropagation();
      e = e.changedTouches ? e.changedTouches[0] : e;
      startDragMousePos = e.pageX;
      startVal = this.values[1];
      this.isDragingUpper = true;
      this.isDragging = true;
      window.addEventListener('mousemove', this.onDrag);
      window.addEventListener('touchmove', this.onDrag);
      window.addEventListener('mouseup', this.onUp);
      window.addEventListener('touchend', this.onUp);
    },
    onDrag: function onDrag(e) {
      var _this = this;

      if (this.disabled) return;
      e.preventDefault();
      e.stopPropagation();
      if (!this.isDragging) return;
      e = e.changedTouches ? e.changedTouches[0] : e;
      window.requestAnimationFrame(function () {
        var diff = (e.pageX - startDragMousePos) / _this.$el.offsetWidth * (_this.max - _this.min);
        var nextVal = startVal + diff;
        if (_this.isDragging) {
          if (_this.isDragingUpper) {
            _this.updateValue([null, nextVal]);
          } else {
            _this.updateValue([nextVal, null]);
          }
        }
      });
    },
    onUp: function onUp(e) {
      e.preventDefault();
      e.stopPropagation();
      this.stopDrag();
    },
    stopDrag: function stopDrag() {
      this.isDragging = false;
      this.isDragingUpper = false;
      window.removeEventListener('mousemove', this.onDrag);
      window.removeEventListener('touchmove', this.onDrag);
      window.removeEventListener('mouseup', this.onUp);
      window.removeEventListener('touchend', this.onUp);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-1f0cfa72","hasScoped":false,"optionsId":"0","buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/slider/slider.vue
var slider_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"slider",class:{'is-disabled': _vm.disabled}},[(_vm.dots)?_vm._l((_vm.stops),function(item,index){return _c('span',{key:index,staticClass:"slider__dot",style:({'left': item + '%'})})}):_vm._e(),_vm._v(" "),(_vm.range)?[_c('div',{staticClass:"slider__bar",style:(_vm.barStyle)}),_vm._v(" "),_c('div',{staticClass:"slider__handle is-lower hint hint--top",class:{
        'is-active': _vm.isDragging && !_vm.isDragingUpper
      },style:({'left': _vm.lowerHandlePosition + '%'}),attrs:{"data-hint":_vm.format(_vm.values[0])}},[_c('span',{on:{"mousedown":_vm.startLowerDrag,"touchstart":_vm.startLowerDrag}})]),_vm._v(" "),_c('div',{staticClass:"slider__handle is-higher hint hint--top",class:{
        'is-active': _vm.isDragging && _vm.isDragingUpper
      },style:({'left': _vm.upperHandlePosition + '%'}),attrs:{"data-hint":_vm.format(_vm.values[1])}},[_c('span',{on:{"mousedown":_vm.startUpperDrag,"touchstart":_vm.startUpperDrag}})])]:[_c('div',{staticClass:"slider__bar",style:(_vm.barStyle)}),_vm._v(" "),_c('div',{staticClass:"slider__handle hint hint--top",class:{
        'is-active': _vm.isDragging
      },style:({'left': _vm.lowerHandlePosition + '%'}),attrs:{"data-hint":_vm.format(_vm.values[0])}},[_c('span',{on:{"mousedown":_vm.startLowerDrag,"touchstart":_vm.startLowerDrag}})])]],2)}
var slider_staticRenderFns = []

// CONCATENATED MODULE: ./src/components/slider/slider.vue
/* script */


/* template */

/* template functional */
var slider___vue_template_functional__ = false
/* styles */
var slider___vue_styles__ = null
/* scopeId */
var slider___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var slider___vue_module_identifier__ = null

var slider_Component = normalizeComponent(
  slider,
  slider_render,
  slider_staticRenderFns,
  slider___vue_template_functional__,
  slider___vue_styles__,
  slider___vue_scopeId__,
  slider___vue_module_identifier__
)

/* harmony default export */ var slider_slider = (slider_Component.exports);

// CONCATENATED MODULE: ./src/components/slider/index.js



/* harmony default export */ var components_slider = (slider_slider);
// CONCATENATED MODULE: ./src/index.js

// Directives


// Layouts





// Core










// Navigations








// Feedback







// Form



var src_install = function install(Vue) {
  // Directives
  Vue.directive(directives_tooltip.name, directives_tooltip);

  // Layouts
  Vue.component(app.name, app);
  Vue.component(sider.name, sider);
  Vue.component(brand.name, brand);
  Vue.component(layout_content.name, layout_content);
  Vue.component(header.name, header);
  Vue.component(body.name, body);
  Vue.component(main.name, main);
  Vue.component(layout_footer.name, layout_footer);
  Vue.component(row.name, row);
  Vue.component(grid_col.name, grid_col);
  Vue.component(level.name, level);
  Vue.component(level_item.name, level_item);
  Vue.component(components_stack.name, components_stack);

  // Core
  Vue.component(badge.name, badge);
  Vue.component(components_tag.name, components_tag);
  Vue.component(box.name, box);
  Vue.component(components_note.name, components_note);
  Vue.component(button_button.name, button_button);
  Vue.component(button_group.name, button_group);
  Vue.component(panel.name, panel);
  Vue.component(components_icon.name, components_icon);
  Vue.component(components_divider.name, components_divider);
  Vue.component(progress_progress_bar.name, progress_progress_bar);
  Vue.component(progress_progress_circle.name, progress_progress_circle);

  // Navigations
  Vue.component(Nav.name, Nav);
  Vue.component(NavItem.name, NavItem);
  Vue.component(menu_menu.name, menu_menu);
  Vue.component(menu_menu_heading.name, menu_menu_heading);
  Vue.component(menu_menu_item.name, menu_menu_item);
  Vue.component(menu_menu_divider.name, menu_menu_divider);
  Vue.component(menu_submenu.name, menu_submenu);
  Vue.component(components_pagination.name, components_pagination);
  Vue.component(steps.name, steps);
  Vue.component(steps_item.name, steps_item);
  Vue.component(dropdown.name, dropdown);
  Vue.component(Dropmenu.name, Dropmenu);
  Vue.component(DropmenuItem.name, DropmenuItem);
  Vue.component(DropmenuDivider.name, DropmenuDivider);
  Vue.component(tabs.name, tabs);
  Vue.component(tabs_tabs_pane.name, tabs_tabs_pane);

  // Forms
  Vue.component(form_form.name, form_form);
  Vue.component(form_input.name, form_input);
  Vue.component(addon.name, addon);
  Vue.component(group.name, group);
  Vue.component(form_field.name, form_field);
  Vue.component(components_form_radio.name, components_form_radio);
  Vue.component(components_form_checkbox.name, components_form_checkbox);
  Vue.component(components_form_switch.name, components_form_switch);
  Vue.component(components_form_select.name, components_form_select);
  Vue.component(components_form_textarea.name, components_form_textarea);
  Vue.component(form_counter.name, form_counter);
  Vue.component(components_slider.name, components_slider);

  // Feedback
  Vue.component(components_popup.name, components_popup);
  Vue.component(components_modal.name, components_modal);
  Vue.component(flash_flash.name, flash_flash);
  Vue.component(components_toast.name, components_toast);
  Vue.prototype.$toast = {
    info: components_toast.info,
    succeed: components_toast.succeed,
    failed: components_toast.failed,
    warn: components_toast.warn,
    loading: components_toast.loading
  };
  Vue.prototype.$notify = notify;

  Vue.prototype.$alert = components_alert;
};
/* global VERSION */
var Cover = {
  install: src_install,
  version: "2.0.3"
};

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(Cover);
}

/* harmony default export */ var src = __webpack_exports__["default"] = (Cover);

/***/ }),

/***/ "lf+9":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "lktj":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("Ibhu");
var enumBugKeys = __webpack_require__("xnc9");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "mClu":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("kM2E");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__("+E39"), 'Object', { defineProperty: __webpack_require__("evD5").f });


/***/ }),

/***/ "n0T6":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("Ibhu");
var hiddenKeys = __webpack_require__("xnc9").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "nvbp":
/***/ (function(module, exports) {

var nestRE = /^(attrs|props|on|nativeOn|class|style|hook)$/

module.exports = function mergeJSXProps (objs) {
  return objs.reduce(function (a, b) {
    var aa, bb, key, nestedKey, temp
    for (key in b) {
      aa = a[key]
      bb = b[key]
      if (aa && nestRE.test(key)) {
        // normalize class
        if (key === 'class') {
          if (typeof aa === 'string') {
            temp = aa
            a[key] = aa = {}
            aa[temp] = true
          }
          if (typeof bb === 'string') {
            temp = bb
            b[key] = bb = {}
            bb[temp] = true
          }
        }
        if (key === 'on' || key === 'nativeOn' || key === 'hook') {
          // merge functions
          for (nestedKey in bb) {
            aa[nestedKey] = mergeFn(aa[nestedKey], bb[nestedKey])
          }
        } else if (Array.isArray(aa)) {
          a[key] = aa.concat(bb)
        } else if (Array.isArray(bb)) {
          a[key] = [aa].concat(bb)
        } else {
          for (nestedKey in bb) {
            aa[nestedKey] = bb[nestedKey]
          }
        }
      } else {
        a[key] = b[key]
      }
    }
    return a
  }, {})
}

function mergeFn (a, b) {
  return function () {
    a && a.apply(this, arguments)
    b && b.apply(this, arguments)
  }
}


/***/ }),

/***/ "pFYg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__("Zzip");

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__("5QVw");

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),

/***/ "qio6":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("evD5");
var anObject = __webpack_require__("77Pl");
var getKeys = __webpack_require__("lktj");

module.exports = __webpack_require__("+E39") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "sB3e":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("52gC");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "tV5f":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "uqUo":
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__("kM2E");
var core = __webpack_require__("FeBl");
var fails = __webpack_require__("S82l");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "vFc/":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("TcQ7");
var toLength = __webpack_require__("QRG4");
var toAbsoluteIndex = __webpack_require__("fkB2");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "vIB/":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("O4g8");
var $export = __webpack_require__("kM2E");
var redefine = __webpack_require__("880/");
var hide = __webpack_require__("hJx8");
var has = __webpack_require__("D2L2");
var Iterators = __webpack_require__("/bQp");
var $iterCreate = __webpack_require__("94VQ");
var setToStringTag = __webpack_require__("e6n0");
var getPrototypeOf = __webpack_require__("PzxK");
var ITERATOR = __webpack_require__("dSzd")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "woOf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__("V3tA"), __esModule: true };

/***/ }),

/***/ "wxAW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__("C4MV");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "xGkn":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("4mcu");
var step = __webpack_require__("EGZi");
var Iterators = __webpack_require__("/bQp");
var toIObject = __webpack_require__("TcQ7");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("vIB/")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "xnc9":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "zQR9":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__("h65t")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__("vIB/")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ })

/******/ });
});